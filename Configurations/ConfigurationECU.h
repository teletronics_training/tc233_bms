/**
 * \file Configuration.h
 * \brief Global configuration
 *
 * \version iLLD_Demos_0_1_0_11
 * \copyright Copyright (c) 2014 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Demo_QspiDmaDemo_SrcDoc_Config Application configuration
 * \ingroup IfxLld_Demo_QspiDmaDemo_SrcDoc
 *
 *
 */

#ifndef CONFIGURATIONECU_H
#define CONFIGURATIONECU_H

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/
#include "Ifx_Cfg.h"
#include "IfxQspi_PinMap.h"
#include "IfxPort_PinMap.h"
#include "IfxAsclin_PinMap.h"
#include "Configuration.h"
#include "ConfigurationIsr.h"


/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

#if defined(AurixBoard_TC265)

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/
/* set here the used pins for QSPI1 */
#define QSPI0_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI0_SCLK_PIN              IfxQspi0_SCLK_P20_11_OUT
#define QSPI0_MTSR_PIN              IfxQspi0_MTSR_P20_14_OUT      // for Application Kit TC2X7
#define QSPI0_MRST_PIN              IfxQspi0_MRSTA_P20_12_IN
#define QSPI0_USE_DMA  // comment line for not using DMA
#define DMA_CH_QSPI0_TX             TFT_DMA_CH_TXBUFF_TO_TXFIFO
#define DMA_CH_QSPI0_RX             TFT_DMA_CH_RXBUFF_FROM_RXFIFO
#define QSPI0_TRANSMIT_CALLBACK     tft_transmit_callback

/* set here the used pins for QSPI1 */
#define QSPI1_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI1_SCLK_PIN	            IfxQspi1_SCLK_P10_2_OUT
//#define QSPI2_MTSR_PIN	            IfxQspi2_MTSR_P15_5_OUT  // for Application Kit TC234L
#define QSPI1_MTSR_PIN	            IfxQspi1_MTSR_P10_1_OUT      // for Application Kit TC2X7
#define QSPI1_MRST_PIN	            IfxQspi1_MRSTB_P11_3_IN

/* set here the used pins for QSPI2 */
#define QSPI2_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI2_SCLK_PIN	            IfxQspi2_SCLK_P15_3_OUT
//#define QSPI2_MTSR_PIN	            IfxQspi2_MTSR_P15_5_OUT  // for Application Kit TC234L
#define QSPI2_MTSR_PIN	            IfxQspi2_MTSR_P15_6_OUT      // for Application Kit TC2X7
#define QSPI2_MRST_PIN	            IfxQspi2_MRSTB_P15_7_IN
//#define QSPI2_USE_DMA  // uncomment line for using DMA
//#define DMA_CH_QSPI2_TX             TLF_DMA_CH_TXBUFF_TO_TXFIFO
//#define DMA_CH_QSPI2_RX             TLF_DMA_CH_RXBUFF_FROM_RXFIFO

/* set here the used pins for QSPI3 */
#define QSPI3_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI3_SCLK_PIN              IfxQspi3_SCLK_P22_3_OUT
//#define QSPI2_MTSR_PIN                IfxQspi2_MTSR_P15_5_OUT  // for Application Kit TC234L
#define QSPI3_MTSR_PIN              IfxQspi3_MTSR_P22_0_OUT      // for Application Kit TC2X7
#define QSPI3_MRST_PIN              IfxQspi3_MRSTE_P22_1_IN


#define TLE9180_QSPI_INIT           qspi3_init
#define TLE9180_USE_CHIPSELECT      IfxQspi3_SLSO12_P22_2_OUT


#define Drv8301_QSPI_INIT           qspi3_init
#define Drv8301_USE_CHIPSELECT      IfxQspi3_SLSO12_P22_2_OUT

#define TLF_QSPI_INIT				qspi2_init
#define TLF_USE_CHIPSELECT          IfxQspi2_SLSO1_P14_2_OUT

#define LCD_QSPI_INIT 				qspi1_init
#define LCD_ENABLE					IfxQspi1_SLSO10_P10_0_OUT
#define LCD_STROBE					&MODULE_P10, 7

#define TFT_QSPI_INIT               qspi0_init
#define TFT_USE_CHIPSELECT          IfxQspi0_SLSO8_P20_6_OUT
#define TFT_USE_SCLK                QSPI0_SCLK_PIN

#define TFT_UPDATE_IRQ              MODULE_SRC.GPSR.GPSR[0].SR0

#define TOUCH_QSPI_INIT             qspi0_init
#define TOUCH_USE_CHIPSELECT        IfxQspi0_SLSO9_P20_3_OUT
#define TOUCH_USE_INT               IfxPort_P10_7

#define CAN_TXD_USE                 IfxPort_P20_8
#define CAN_RXD_USE                 IfxPort_P20_7

#define LIN_TXD_USE                 IfxPort_P15_0
#define LIN_RXD_USE                 IfxPort_P15_1

#define ASC_TXD_USE                 IfxAsclin0_TX_P14_0_OUT
#define ASC_RXD_USE                 IfxAsclin0_RXA_P14_1_IN

#define LED1 						&MODULE_P33, 8
#define LED2 						&MODULE_P33, 9
#define LED3 						&MODULE_P33, 10
#define LED4 						&MODULE_P33, 11

#define MCU_EN 						&MODULE_P20, 10
#define MCU_SOFF 					&MODULE_P20, 9

#define DEBUG_TOGGLE				&MODULE_P14, 9

//#define HALL_A 						&MODULE_P02, 3
//#define HALL_B 						&MODULE_P02, 5
//#define HALL_C 						&MODULE_P02, 4

#define HALL_A          IfxGtm_TIM0_3_TIN3_P02_3_IN     /* Input port pin for the PWM signal */
#define HALL_B          IfxGtm_TIM0_4_TIN4_P02_4_IN     /* Input port pin for the PWM signal */
#define HALL_C          IfxGtm_TIM0_5_TIN5_P02_5_IN     /* Input port pin for the PWM signal */
#elif defined(AurixBoard_TC233)

#define POT_ENABLE                  IfxQspi0_SLSO3_P11_10_OUT //IfxQspi2_SLSO10_P34_3_OUT
#define POT_NRS                     &MODULE_P20, 8

#define RELAY00                     &MODULE_P33, 5
#define RELAY01                     NULL, 2
#define RELAY02                     &MODULE_P13, 0
#define RELAY03                     &MODULE_P13, 1
#define RELAY04                     &MODULE_P13, 2
#define RELAY05                     &MODULE_P13, 3
#define RELAY06                     &MODULE_P33, 8
#define RELAY07                     &MODULE_P33, 6

#define MCU_IO0                     &MODULE_P02, 4
#define MCU_IO1                     NULL, 2
#define MCU_IO2                     NULL, 3
#define MCU_IO3                     &MODULE_P33, 7
#define MCU_IO4                     &MODULE_P15, 1
#define MCU_IO5                     &MODULE_P23, 1
#define MCU_IO6                     NULL, 5
#define MCU_IO7                     &MODULE_P20, 11
#define MCU_IO8                     &MODULE_P20, 10
#define MCU_IO9                     NULL,0
#define MCU_IO10                    &MODULE_P33, 9

#define MCU_PWM0                    IfxGtm_TOM1_6_TOUT100_P11_11_OUT
#define MCU_PWM1                    IfxGtm_TOM1_4_TOUT98_P11_9_OUT
#define MCU_PWM2                    IfxGtm_TOM1_2_TOUT96_P11_3_OUT
#define MCU_PWM3                    IfxGtm_TOM1_3_TOUT97_P11_6_OUT
#define MCU_PWM4                    IfxGtm_TOM1_1_TOUT95_P11_2_OUT

#define PWM_GPIO_Period             50000

#else
#error AurixBoard selection required.
#endif

#endif
