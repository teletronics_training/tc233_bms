/*
 * PWM_GPIO.c
 *
 *  Created on: Oct 2, 2021
 *      Author: weetit
 */


#include "PWM_GPIO.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Include here

//Declaration here

#define ISR_PRIORITY_TOM    20                                      /* Interrupt priority number                    */
#define LED                 IfxGtm_TOM1_6_TOUT100_P11_11_OUT          /* LED which will be driven by the PWM          */
#define PWM_PERIOD          100                                   /* PWM period for the TOM                       */
#define FADE_STEP           PWM_PERIOD / 100                        /* PWM duty cycle for the TOM                   */

IfxGtm_Tom_Pwm_Config g_tomConfig;                                  /* Timer configuration structure                */
IfxGtm_Tom_Pwm_Driver g_tomDriver;                                  /* Timer Driver structure                       */

//Global variable

App_PWM_GPIO g_PWM_GPIO[PWM_GPIO_Num]=
{
        {
                .PWM_GPIO=&MCU_PWM0,
                .dutyCycle=0
        },
        {
                .PWM_GPIO=&MCU_PWM1,
                .dutyCycle=0
        },
        {
                .PWM_GPIO=&MCU_PWM2,
                .dutyCycle=0
        },
        {
                .PWM_GPIO=&MCU_PWM3,
                .dutyCycle=0
        },
        {
                .PWM_GPIO=&MCU_PWM4,
                .dutyCycle=0
        },
};


uint32 g_fadeValue = 0;                                             /* Fade value, starting from 0                  */
sint8 g_fadeDir = 1;                                                /* Fade direction variable                      */

void PWM_GPIO_init(void)
{
    IfxGtm_enable(&MODULE_GTM);                                     /* Enable GTM                                   */

    IfxGtm_Cmu_enableClocks(&MODULE_GTM, IFXGTM_CMU_CLKEN_FXCLK);   /* Enable the FXU clock                         */

    int loop=0;
    for(loop=0;loop<PWM_GPIO_Num;loop++)
    {
        IfxGtm_Tom_Pwm_initConfig(&g_PWM_GPIO[loop].tomConfig, &MODULE_GTM);
        g_PWM_GPIO[loop].tomConfig.tom = g_PWM_GPIO[loop].PWM_GPIO->tom;                                      /* Select the TOM depending on the LED          */
        g_PWM_GPIO[loop].tomConfig.tomChannel = g_PWM_GPIO[loop].PWM_GPIO->channel;                           /* Select the channel depending on the LED      */
        g_PWM_GPIO[loop].tomConfig.period = PWM_PERIOD;                                /* Set the timer period                         */
        g_PWM_GPIO[loop].tomConfig.pin.outputPin = g_PWM_GPIO[loop].PWM_GPIO;                               /* Set the LED port pin as output               */
        g_PWM_GPIO[loop].tomConfig.synchronousUpdateEnabled = TRUE;                    /* Enable synchronous update                    */
        g_PWM_GPIO[loop].tomConfig.dutyCycle = g_PWM_GPIO[loop].dutyCycle;
        IfxGtm_Tom_Pwm_init(&g_PWM_GPIO[loop].tomDriver, &g_PWM_GPIO[loop].tomConfig);                /* Initialize the GTM TOM                       */
        IfxGtm_Tom_Pwm_start(&g_PWM_GPIO[loop].tomDriver, TRUE);                       /* Start the PWM                                */
    }
}

void PWM_GPIO_setDuty(App_PWM_GPIO_Port gpio,uint32 dutyCycle)
{
    g_PWM_GPIO[gpio].tomConfig.dutyCycle = (100-dutyCycle)*g_PWM_GPIO[gpio].tomConfig.period/100;                              /* Change the value of the duty cycle           */
    IfxGtm_Tom_Pwm_init(&g_PWM_GPIO[gpio].tomDriver, &g_PWM_GPIO[gpio].tomConfig);                /* Re-initialize the PWM                        */
}

//Code here

