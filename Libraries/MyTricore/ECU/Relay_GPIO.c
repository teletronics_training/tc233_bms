/*
 * Relay_GPIO.c
 *
 *  Created on: Oct 2, 2021
 *      Author: weetit
 */


#include "Relay_GPIO.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Include here

//Declaration here

//Global variable

App_Relay_GPIO_OutPin g_App_Relay_GPIO_OutPin[Relay_GPIO_Num]={
        {
                RELAY00,IfxPort_State_low,FALSE
        },
        {
                RELAY01,IfxPort_State_low,FALSE
        },
        {
                RELAY02,IfxPort_State_low,FALSE
        },
        {
                RELAY03,IfxPort_State_low,FALSE
        },
        {
                RELAY04,IfxPort_State_low,FALSE
        },
        {
                RELAY05,IfxPort_State_low,FALSE
        },
        {
                RELAY06,IfxPort_State_low,FALSE
        },
        {
                RELAY07,IfxPort_State_low,FALSE
        }
};
//Code here

void Relay_GPIO_init(void)
{
    int loop;
    for(loop=0;loop<Relay_GPIO_Num;loop++)
    {
        if(g_App_Relay_GPIO_OutPin[loop].port!=NULL)
        {
            IfxPort_setPinMode(g_App_Relay_GPIO_OutPin[loop].port, g_App_Relay_GPIO_OutPin[loop].pinIndex, IfxPort_Mode_outputPushPullGeneral);
        }
        Relay_GPIO_off(loop);
    }
}

void Relay_GPIO_on(App_Relay_GPIO_Port gpio)
{
    if(g_App_Relay_GPIO_OutPin[gpio].port!=NULL)
    {
        IfxPort_setPinState(g_App_Relay_GPIO_OutPin[gpio].port, g_App_Relay_GPIO_OutPin[gpio].pinIndex
                , IfxPort_State_low);
        g_App_Relay_GPIO_OutPin[gpio].status=TRUE;
    }
}

void Relay_GPIO_off(App_Relay_GPIO_Port gpio)
{
    if(g_App_Relay_GPIO_OutPin[gpio].port!=NULL)
    {
        IfxPort_setPinState(g_App_Relay_GPIO_OutPin[gpio].port, g_App_Relay_GPIO_OutPin[gpio].pinIndex
                , IfxPort_State_high);
        g_App_Relay_GPIO_OutPin[gpio].status=FALSE;
    }
}

