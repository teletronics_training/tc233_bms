/*
 * FlashMem.c
 *
 *  Created on: Sep 23, 2021
 *      Author: weetit
 */


#include "FlashMem.h"
#include "Configuration.h"
#include "SysSe/Bsp/Bsp.h"
#include "MyBsp.h"
#include "FlashDevice.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Include here

//Declaration here


#if defined(AurixBoard_TC265)

#elif defined(AurixBoard_TC233)

#else
#error AurixBoard selection required.
#endif



//Global variable
//App_Qspi_Cpu g_Qspi_Cpu;

//Code here

void Flash_init(void)
{
    /* disable interrupts */
    boolean interruptState = IfxCpu_disableInterrupts();
    // install interrupt handlers
//    IfxCpu_Irq_installInterruptHandler(&qspi1TxISR, ISR_PRIORITY_QSPI1_TX);
//    IfxCpu_Irq_installInterruptHandler(&qspi1RxISR, ISR_PRIORITY_QSPI1_RX);
//    IfxCpu_Irq_installInterruptHandler(&qspi1ErISR, ISR_PRIORITY_QSPI1_ER);
//    IfxCpu_enableInterrupts();

    // create module config
    IfxQspi_SpiMaster_Config spiMasterConfig;
#if defined(AurixBoard_TC265)

#elif defined(AurixBoard_TC233)
    IfxQspi_SpiMaster_initModuleConfig(&spiMasterConfig, &MODULE_QSPI0);
#else
#error AurixBoard selection required.
#endif


    // set the desired mode and maximum baudrate
    spiMasterConfig.base.mode             = SpiIf_Mode_master;
    spiMasterConfig.base.maximumBaudrate  = 5000000;

    // ISR priorities and interrupt target

#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
    spiMasterConfig.base.txPriority       = ISR_PRIORITY_QSPI0_TX;
    spiMasterConfig.base.rxPriority       = ISR_PRIORITY_QSPI0_RX;
    spiMasterConfig.base.erPriority       = ISR_PRIORITY_QSPI0_ER;
    spiMasterConfig.base.isrProvider      = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());
#else
#error AurixBoard selection required.
#endif

    // pin configuration
#if defined(AurixBoard_TC265)
    const IfxQspi_SpiMaster_Pins pins = {
        &QSPI1_SCLK_PIN, IfxPort_OutputMode_pushPull, // SCLK
        &QSPI1_MTSR_PIN, IfxPort_OutputMode_pushPull, // MTSR
        &QSPI1_MRST_PIN, IfxPort_InputMode_pullDown,  // MRST
        IfxPort_PadDriver_cmosAutomotiveSpeed3 // pad driver mode
    };
#elif defined(AurixBoard_TC233)
    const IfxQspi_SpiMaster_Pins pins = {
        &QSPI0_SCLK_PIN, IfxPort_OutputMode_pushPull, // SCLK
        &QSPI0_MTSR_PIN, IfxPort_OutputMode_pushPull, // MTSR
        &QSPI0_MRST_PIN, IfxPort_InputMode_pullDown,  // MRST
        IfxPort_PadDriver_cmosAutomotiveSpeed3 // pad driver mode
    };
#else
#error AurixBoard selection required.
#endif


    spiMasterConfig.pins = &pins;

    // initialize module
    IfxQspi_SpiMaster spi; // defined globally
    IfxQspi_SpiMaster_initModule(&g_Qspi_Cpu.drivers.spiMaster, &spiMasterConfig);

    // create channel config
    IfxQspi_SpiMaster_ChannelConfig spiMasterChannelConfig;
    IfxQspi_SpiMaster_initChannelConfig(&spiMasterChannelConfig, &g_Qspi_Cpu.drivers.spiMaster);

    // set the baudrate for this channel
    spiMasterChannelConfig.base.baudrate = 5000000;
    spiMasterChannelConfig.base.mode.csTrailDelay = 2;
    spiMasterChannelConfig.base.mode.csInactiveDelay = 2;
    spiMasterChannelConfig.base.mode.shiftClock = SpiIf_ShiftClock_shiftTransmitDataOnTrailingEdge;

    // select pin configuration
    const IfxQspi_SpiMaster_Output slsOutput = {
        &FLASH_ENABLE,
        IfxPort_OutputMode_pushPull,
        IfxPort_PadDriver_cmosAutomotiveSpeed1
    };
    spiMasterChannelConfig.sls.output = slsOutput;

    // initialize channel
    IfxQspi_SpiMaster_initChannel(&g_Qspi_Cpu.drivers.spiMasterFlashChannel, &spiMasterChannelConfig);
    IfxCpu_restoreInterrupts(interruptState);

}

#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)

//IFX_INTERRUPT(qspi0TxISR, 0, ISR_PRIORITY_QSPI0_TX)
//{
//    IfxQspi_SpiMaster_isrTransmit(&g_Qspi_Cpu.drivers.spiMaster);
//}
//
//IFX_INTERRUPT(qspi0RxISR, 0, ISR_PRIORITY_QSPI0_RX)
//{
//    IfxQspi_SpiMaster_isrReceive(&g_Qspi_Cpu.drivers.spiMaster);
//}
//
//IFX_INTERRUPT(qspi0ErISR, 0, ISR_PRIORITY_QSPI0_ER)
//{
//    IfxQspi_SpiMaster_isrError(&g_Qspi_Cpu.drivers.spiMaster);
//}

#else
#error AurixBoard selection required.
#endif



