/*
 * LTC6813_CMD.h
 *
 *  Created on: Aug 6, 2020
 *      Author: weeti
 */

#ifndef LTC6813_CMD_H_
#define LTC6813_CMD_H_

#include <Cpu/Std/Ifx_Types.h>
#include "Configuration.h"


#define NUM_RX_BYT 8

typedef enum {
	CELL=1,
	AUX=2,
	STAT=3,
	CFGR=0,
	CFGRB=4
}PEC_TYPE;

typedef enum {
	WRCFGA=0x0001,
	WRCFGB=0x0024,
	RDCFGA=0x0002,
	RDCFGB=0x0026,
	RDCVA=0x0004,
	RDCVB=0x0006,
	RDCVC=0x0008,
	RDCVD=0x000A,
	RDCVE=0x0009,
	RDCVF=0x000B,
	RDAUXA=0x000C,
	RDAUXB=0x000E,
	RDAUXC=0x000D,
	RDAUXD=0x000F,
	RDSTATA=0x0010,
	RDSTATB=0x0012,
	WRSCTRL=0x0014,
	WRPWM=0x0020,
	WRPSB=0x001C,
	RDSCTRL=0x0016,
	RDPWM=0x0022,
	RDPSB=0x001E
}LTC6813_CMD;

typedef enum {
	MD_422HZ_1KHZ=0,
	MD_27KHZ_14KHZ=1,
	MD_7KHZ_3KHZ=2,
	MD_26HZ_2KHZ=3
}LTC6813_MD;

typedef enum {
	ADC_OPT_DISABLED=0,
	ADC_OPT_ENABLED=1
}LTC6813_ADCOPT_EN;

typedef enum {
	CELL_CH_ALL=0,
	CELL_CH_1and7=1,
	CELL_CH_2and8=2,
	CELL_CH_3and9=3,
	CELL_CH_4and10=4,
	CELL_CH_5and11=5,
	CELL_CH_6and12=6
}LTC6813_CELL_CH;

typedef enum {
	SELFTEST_1=1,
	SELFTEST_2=2
}LTC6813_SELFTEST;

typedef enum {
	AUX_CH_ALL=0,
	AUX_CH_GPIO1=1,
	AUX_CH_GPIO2=2,
	AUX_CH_GPIO3=3,
	AUX_CH_GPIO4=4,
	AUX_CH_GPIO5=5,
	AUX_CH_VREF2=6
}LTC6813_AUX_CH;

typedef enum {
	STAT_CH_ALL=0,
	STAT_CH_SOC=1,
	STAT_CH_ITEMP=2,
	STAT_CH_VREGA=3,
	STAT_CH_VREGD=4
}LTC6813_STAT_CH;

typedef enum {
	REG_ALL=0,
	REG_1=1,
	REG_2=2,
	REG_3=3,
	REG_4=4,
	REG_5=5,
	REG_6=6
}LTC6813_REG;

typedef enum {
	DCP_DISABLED=0,
	DCP_ENABLED=1
}LTC6813_DCP;

typedef enum {
	PULL_DOWN_CURRENT=0,
	PULL_UP_CURRENT=1
}LTC6813_ADOW_PULL_UP_DOWN;

typedef enum {
	PULL_DOWN_ON=0,			//GPIO pull down on
	PULL_DOWN_OFF=1			//GPIO pull down off (default)
}LTC6813_GPIO_PULL_DOWN;

typedef enum {
	REF_ON_CONV=0,			//Vref on while conversion (default)
	REF_ON_ALWAYS=1			//Vref on except watchdog time out
}LTC6813_REFON;

typedef enum {
	DT_DISABLE=0,		//Discharge timer flag read only (default)
	DT_ENABLE=1		//
}LTC6813_DTEN;

typedef enum {
	ADCOPT_FAST=0,		//Use with MD[1:0] for 27kHz, 7kHz 422Hz or 26Hz (default)
	ADCOPT_SLOW=1		//Use with MD[1:0] for 14kHz, 3kHz 1kHz or 2kHz
}LTC6813_ADCOPT;

typedef uint16 LTC6813_VUV;
typedef uint16 LTC6813_VOV;

typedef enum {
	DISCHARGE_DISABLE=0,		//Discharge Cell (default)
	DISCHARGE_ENABLE=1
}LTC6813_DCC;

IFX_EXTERN LTC6813_DCC DCC[19]; //DCC[0] for GPIO9 Pull Down, DCC[1-18] for cell discharge

typedef enum {
	DCTO_DISABLE=0,		//Discharge Disable or Time Out
	DCTO_0_5min=1,
	DCTO_1min=2,
	DCTO_2min=3,
	DCTO_3min=4,
	DCTO_4min=5,
	DCTO_5min=6,
	DCTO_10min=7,
	DCTO_15min=8,
	DCTO_20min=9,
	DCTO_30min=0xA,
	DCTO_40min=0xB,
	DCTO_60min=0xC,
	DCTO_75min=0xD,
	DCTO_90min=0xE,
	DCTO_120min=0xF
}LTC6813_DCTO;

typedef enum {
	MUTE_DEACTIVATE=0,		//Mute Activate and Discharging Disable
	MUTE_ACTIVATE=1			//Mute is Deactivate
}LTC6813_MUTE_STATUS;		//Read Only

typedef enum {
	NORMAL_DR=0,			//Digital Redundancy compare normally
	FORCE_DR_FAIL=1			//Digital Redundancy is forced to fail
}LTC6813_FDRF;

typedef enum {
	PS_ALL=0,				//ADC1, ADC2 and ADC3
	PS_ADC1=1,				//ADC1
	PS_ADC2=2,				//ADC2
	PS_ADC3=3,				//ADC3
}LTC6813_PS;

typedef enum {				//Monitor cell @ 7kHz every 30sec
	DTM_DISABLE=0,			//Discharge timer monitor disable
	DTM_ENABLE=1			//Discharge timer monitor enable
}LTC6813_DTMEN;

// Cell Voltage data structure.
typedef struct
{
	uint16 c_codes[18];// Cell Voltage Codes
	uint8 pec_match[6];// If a PEC error was detected during most recent read cmd
} LTC6813_cv;

// AUX Reg Voltage Data
typedef struct
{
	uint16 a_codes[9];// Aux Voltage Codes
	uint8 pec_match[4];// If a PEC error was detected during most recent read cmd
} LTC6813_ax;

//Status Reg data structure.
typedef struct
{
	uint16 stat_codes[4];// A two dimensional array of the stat voltage codes.
	uint8 flags[3]; // byte array that contains the uv/ov flag data
	uint8 mux_fail[1]; // Mux self test status flag
	uint8 thsd[1]; // Thermal shutdown status
	uint8 pec_match[2];// If a PEC error was detected during most recent read cmd
} LTC6813_st;

typedef struct
{
	uint8 tx_data[6];
	uint8 rx_data[8];
	uint8 rx_pec_match;// If a PEC error was detected during most recent read cmd
} LTC6813_ic_register;

typedef struct
{
	uint16 pec_count;
	uint16 cfgr_pec;
	uint16 cell_pec[6];
	uint16 aux_pec[4];
	uint16 stat_pec[2];
} LTC6813_pec_counter;

typedef struct
{
	uint8 cell_channels;
	uint8 stat_channels;
	uint8 aux_channels;
	uint8 num_cv_reg;
	uint8 num_gpio_reg;
	uint8 num_stat_reg;
} LTC6813_register_cfg;

typedef struct
{
	LTC6813_ic_register config;
	LTC6813_ic_register configb;
	LTC6813_cv   cells;
	LTC6813_ax   aux;
	LTC6813_st   stat;
	LTC6813_ic_register  com;
	LTC6813_ic_register pwm;
	LTC6813_ic_register pwmb;
	LTC6813_ic_register sctrl;
	LTC6813_ic_register sctrlb;
	boolean isospi_reverse;
	LTC6813_pec_counter crc_count;
	LTC6813_register_cfg ic_reg;
	long system_open_wire;
} LTC6813_cell_asic;

typedef struct
{
    LTC6813_REFON REFON;
    LTC6813_ADCOPT ADCOPT;
    LTC6813_GPIO_PULL_DOWN gpio_pull_down[9]; // Gpio 1,2,3,4,5
    LTC6813_VUV UV;
    LTC6813_VOV OV;
    LTC6813_DCC dccBits[19]; //Dcc 1,2,3,4,5,6,7,8,9,10,11,12
    LTC6813_DCTO dctoBits; //Dcto 0,1,2,3
    LTC6813_FDRF FDRF;
    LTC6813_DTMEN DTMEN;
    LTC6813_PS ps; //ps-0,1
} LTC6813_config;

IFX_EXTERN uint16 pec15Table[256];
IFX_EXTERN uint16 CRC15_POLY;
IFX_EXTERN void init_PEC15_Table();
IFX_EXTERN uint16 pec15 (char *data , int len);

/*! Helper Function to initialize the CFGR data structures*/
IFX_EXTERN void LTC6813_init_cfg(uint8 total_ic,
	LTC6813_cell_asic *ic);

/*! Helper function to set appropriate bits in CFGR register based on bit function*/
IFX_EXTERN void LTC6813_set_cfgr(uint8 nIC,
		LTC6813_cell_asic *ic,
		LTC6813_config config
);

IFX_EXTERN void LTC6813_set_cfgr_refon(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_REFON refon);
IFX_EXTERN void LTC6813_set_cfgr_adcopt(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_ADCOPT adcopt);
IFX_EXTERN void LTC6813_set_cfgr_gpio(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_GPIO_PULL_DOWN gpio[9]);
IFX_EXTERN void LTC6813_set_cfgr_dis(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_DCC dcc[19]);
IFX_EXTERN void LTC6813_set_cfgr_dcto(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_DCTO dcto);
IFX_EXTERN void LTC6813_set_cfgr_uv(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_VUV uv);
IFX_EXTERN void LTC6813_set_cfgr_ov(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_VOV ov);
IFX_EXTERN void LTC6813_set_cfgrb_fdrf(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_FDRF fdrf);
IFX_EXTERN void LTC6813_set_cfgrb_dtmen(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_DTMEN dtmen);
IFX_EXTERN void LTC6813_set_cfgrb_ps(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_PS ps);

IFX_EXTERN void LTC681x_reset_crc_count(uint8 total_ic,
		LTC6813_cell_asic *ic);
IFX_EXTERN void LTC6813_init_reg_limits(uint8 total_ic, LTC6813_cell_asic *ic);
IFX_EXTERN void LTC681x_check_pec(uint8 total_ic,
						PEC_TYPE reg,
						LTC6813_cell_asic *ic
					   );
IFX_EXTERN sint8 parse_cells(uint8 current_ic,
					uint8 cell_reg,
					uint8 cell_data[],
					uint16 *cell_codes,
					uint8 *ic_pec
					);

#endif /* LTC6813_CMD_H_ */
