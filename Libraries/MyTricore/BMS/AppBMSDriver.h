/*
 * AppBMSDriver.h
 *
 *  Created on: Oct 9, 2020
 *      Author: weeti
 */

#ifndef APPBMSDRIVER_H_
#define APPBMSDRIVER_H_

#include "LTC6813.h"
#include "Ifx_Types.h"

typedef struct
{
	int flag;
	float totalV;
	float cell[18*LTC6813_TOTAL_IC];
	float max_voltage;
	int discharge_cell;
	unsigned char sum;
}AppBMSData;

IFX_EXTERN AppBMSData g_AppBMS_data;
IFX_EXTERN unsigned char AppBMS_ChkSumData();
IFX_EXTERN void AppBMS_ConvertCellVoltage();
IFX_EXTERN void AppBMS_ConvertCellVoltage_Discharge(int cell);

#endif /* APPBMSDRIVER_H_ */
