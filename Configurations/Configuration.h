/**
https://www.lazada.co.th/products/solder-paste-relife-rl-403s-138-rl-404s-183-rl-406s-227-i2547152855-s9058831021.html?spm=a2o4m.searchlist.list.2.1af82a6dPG4zh0&search=1 * \file Configuration.h
 * \brief Global configuration
 *
 * \version iLLD_Demos_0_1_0_11
 * \copyright Copyright (c) 2014 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Demo_QspiDmaDemo_SrcDoc_Config Application configuration
 * \ingroup IfxLld_Demo_QspiDmaDemo_SrcDoc
 *
 *
 */

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#define GATEDRV_TLF9180

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/
#include "Ifx_Cfg.h"
#include "IfxQspi_PinMap.h"
#include "IfxPort_PinMap.h"
#include "IfxAsclin_PinMap.h"
#include <Qspi/SpiMaster/IfxQspi_SpiMaster.h>
//#include "Qspi/Qspi2.h"
//#include "Qspi/Qspi1.h"

#include "ConfigurationIsr.h"

//#define IC_Driver_TLE9180
#define IC_Driver_Drv8301

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

#if defined(AurixBoard_TC265)

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/
/* set here the used pins for QSPI1 */
#define QSPI0_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI0_SCLK_PIN              IfxQspi0_SCLK_P20_11_OUT
#define QSPI0_MTSR_PIN              IfxQspi0_MTSR_P20_14_OUT      // for Application Kit TC2X7
#define QSPI0_MRST_PIN              IfxQspi0_MRSTA_P20_12_IN
#define QSPI0_USE_DMA  // comment line for not using DMA
#define DMA_CH_QSPI0_TX             TFT_DMA_CH_TXBUFF_TO_TXFIFO
#define DMA_CH_QSPI0_RX             TFT_DMA_CH_RXBUFF_FROM_RXFIFO
#define QSPI0_TRANSMIT_CALLBACK     tft_transmit_callback

/* set here the used pins for QSPI1 */
#define QSPI1_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI1_SCLK_PIN	            IfxQspi1_SCLK_P10_2_OUT
//#define QSPI2_MTSR_PIN	            IfxQspi2_MTSR_P15_5_OUT  // for Application Kit TC234L
#define QSPI1_MTSR_PIN	            IfxQspi1_MTSR_P10_1_OUT      // for Application Kit TC2X7
#define QSPI1_MRST_PIN	            IfxQspi1_MRSTB_P11_3_IN

/* set here the used pins for QSPI2 */
#define QSPI2_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI2_SCLK_PIN	            IfxQspi2_SCLK_P15_3_OUT
//#define QSPI2_MTSR_PIN	            IfxQspi2_MTSR_P15_5_OUT  // for Application Kit TC234L
#define QSPI2_MTSR_PIN	            IfxQspi2_MTSR_P15_6_OUT      // for Application Kit TC2X7
#define QSPI2_MRST_PIN	            IfxQspi2_MRSTB_P15_7_IN
//#define QSPI2_USE_DMA  // uncomment line for using DMA
//#define DMA_CH_QSPI2_TX             TLF_DMA_CH_TXBUFF_TO_TXFIFO
//#define DMA_CH_QSPI2_RX             TLF_DMA_CH_RXBUFF_FROM_RXFIFO

/* set here the used pins for QSPI3 */
#define QSPI3_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI3_SCLK_PIN              IfxQspi3_SCLK_P22_3_OUT
//#define QSPI2_MTSR_PIN                IfxQspi2_MTSR_P15_5_OUT  // for Application Kit TC234L
#define QSPI3_MTSR_PIN              IfxQspi3_MTSR_P22_0_OUT      // for Application Kit TC2X7
#define QSPI3_MRST_PIN              IfxQspi3_MRSTE_P22_1_IN


#define TLE9180_QSPI_INIT           qspi3_init
#define TLE9180_USE_CHIPSELECT      IfxQspi3_SLSO12_P22_2_OUT


#define Drv8301_QSPI_INIT           qspi3_init
#define Drv8301_USE_CHIPSELECT      IfxQspi3_SLSO12_P22_2_OUT

#define TLF_QSPI_INIT				qspi2_init
#define TLF_USE_CHIPSELECT          IfxQspi2_SLSO1_P14_2_OUT

#define LCD_QSPI_INIT 				qspi1_init
#define LCD_ENABLE					IfxQspi1_SLSO10_P10_0_OUT
#define LCD_STROBE					&MODULE_P10, 7

#define TFT_QSPI_INIT               qspi0_init
#define TFT_USE_CHIPSELECT          IfxQspi0_SLSO8_P20_6_OUT
#define TFT_USE_SCLK                QSPI0_SCLK_PIN

#define TFT_UPDATE_IRQ              MODULE_SRC.GPSR.GPSR[0].SR0

#define TOUCH_QSPI_INIT             qspi0_init
#define TOUCH_USE_CHIPSELECT        IfxQspi0_SLSO9_P20_3_OUT
#define TOUCH_USE_INT               IfxPort_P10_7

#define CAN_TXD_USE                 IfxPort_P20_8
#define CAN_RXD_USE                 IfxPort_P20_7

#define LIN_TXD_USE                 IfxPort_P15_0
#define LIN_RXD_USE                 IfxPort_P15_1

#define ASC_TXD_USE                 IfxAsclin0_TX_P14_0_OUT
#define ASC_RXD_USE                 IfxAsclin0_RXA_P14_1_IN

#define LED1 						&MODULE_P33, 8
#define LED2 						&MODULE_P33, 9
#define LED3 						&MODULE_P33, 10
#define LED4 						&MODULE_P33, 11

#define MCU_EN 						&MODULE_P20, 10
#define MCU_SOFF 					&MODULE_P20, 9

#define DEBUG_TOGGLE				&MODULE_P14, 9

//#define HALL_A 						&MODULE_P02, 3
//#define HALL_B 						&MODULE_P02, 5
//#define HALL_C 						&MODULE_P02, 4

#define HALL_A          IfxGtm_TIM0_3_TIN3_P02_3_IN     /* Input port pin for the PWM signal */
#define HALL_B          IfxGtm_TIM0_4_TIN4_P02_4_IN     /* Input port pin for the PWM signal */
#define HALL_C          IfxGtm_TIM0_5_TIN5_P02_5_IN     /* Input port pin for the PWM signal */
#elif defined(AurixBoard_TC233)
/* set here the used pins for QSPI0 */
#define QSPI0_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI0_SCLK_PIN              IfxQspi0_SCLK_P20_13_OUT
#define QSPI0_MTSR_PIN              IfxQspi0_MTSR_P20_14_OUT
#define QSPI0_MRST_PIN              IfxQspi0_MRSTA_P20_12_IN

/* set here the used pins for QSPI2 */
#define QSPI2_MAX_BAUDRATE          50000000  // maximum baudrate which is possible to get a small timequantum
#define QSPI2_SCLK_PIN              IfxQspi2_SCLK_P15_3_OUT
#define QSPI2_MTSR_PIN              IfxQspi2_MTSR_P15_5_OUT
#define QSPI2_MRST_PIN              IfxQspi2_MRSTA_P15_4_IN

//#define LCD_QSPI_INIT               qspi2_init
#define LCD_ENABLE                  IfxQspi2_SLSO0_P15_2_OUT //IfxQspi2_SLSO10_P34_3_OUT
#define LCD_STROBE                  &MODULE_P15, 1

#define FLASH_ENABLE                IfxQspi0_SLSO1_P20_9_OUT //IfxQspi2_SLSO10_P34_3_OUT


#define LED1                        &MODULE_P13, 0
#define LED2                        &MODULE_P13, 1
#define LED3                        &MODULE_P13, 2
#define LED4                        &MODULE_P13, 3


#else
#error AurixBoard selection required.
#endif

//Setup how to deal with the deadtime
#define USE_GTM_DTM                             (ENABLED)
#if (USE_GTM_DTM == ENABLED)
#define   DTM_USED   (TRUE)
#elif(USE_GTM_DTM == DISABLED)

#endif

#define TLF_BUFFER_SIZE 16   /**< \brief Tx/Rx Buffer size */

typedef struct
{
    uint32 spiTxBuffer[TLF_BUFFER_SIZE];                               /**< \brief Qspi Transmit buffer */
    uint32 spiRxBuffer[TLF_BUFFER_SIZE];                               /**< \brief Qspi receive buffer */
    uint8 *txData;
    uint8 *rxData;
} AppQspi_Buffer;

typedef struct
{
    AppQspi_Buffer qspiBuffer;                       /**< \brief Qspi buffer */
    struct
    {
        IfxQspi_SpiMaster         spiMaster;            /**< \brief Pointer to spi Master handle */
        IfxQspi_SpiMaster_Channel spiMasterFlashChannel;      /**< \brief Spi Master Channel handle */
        IfxQspi_SpiMaster_Channel spiMasterPOTChannel;      /**< \brief Spi Master Channel handle */
        IfxQspi_SpiMaster_Channel spiMasterLTC6813Channel;      /**< \brief Spi Master Channel handle */
    }drivers;
}  App_Qspi_Cpu;

//Add export variable here
IFX_EXTERN App_Qspi_Cpu g_Qspi_Cpu;

#endif
