/*
 * AppScheduler.c
 *
 *  Created on: 19.09.2013
 *
 */


#include "AppScheduler.h"
#include "Ifx_Cfg_Scheduler.h"


void AppScheduler_initTask(AppScheduler_task *handle,AppScheduler_cfg_tasks *config)
{
	handle->taskcounter = config->taskoffset + config->taskreload;
	handle->taskreload = config->taskreload;
	handle->countinous = config->countinous;
	handle->callback = config->callback;
}



 void AppScheduler_tick(AppScheduler_task *handle)
{
	uint32 i;
	for(i=0;i<AppScheduler_Task_count;i++)
	{
		if(handle->taskcounter)
		{

			handle->taskcounter--;
			if(handle->taskcounter == 0)
			{
				if(handle->countinous)
				{
					handle->taskcounter = handle->taskreload;
				}
				if(handle->run != FALSE)
				{
					handle->overflow++;
				}
				handle->run = TRUE;
			}
		}
		handle++;
	}
}



void AppScheduler_process(AppScheduler_task *handle)
{

	if(handle->run != FALSE)
	{
		handle->run = FALSE;
		handle->callback();
	}

}

