/*
 * LTC6813_SMD.c
 *
 *  Created on: Aug 8, 2020
 *      Author: weeti
 */


#include "LTC6813_CMD.h"
#include "Configuration.h"
#include <Qspi/SpiMaster/IfxQspi_SpiMaster.h>
#include "SysSe/Bsp/Bsp.h"
#include "LCD_Char.h"

uint16 pec15Table[256];
uint16 CRC15_POLY = 0x4599;
LTC6813_DCC DCC[19];

void init_PEC15_Table()
{
        uint16 remainder;
        int i,bit;

        for (i = 0; i < 256; i++)
        {
                remainder = i << 7;
                for (bit = 8; bit > 0; --bit)
                {
                        if (remainder & 0x4000)
                        {
                                remainder = ((remainder << 1));
                                remainder = (remainder ^ CRC15_POLY);
                        }
                        else
                        {
                                remainder = ((remainder << 1));
                        }}
                pec15Table[i] = remainder&0xFFFF;
        }
}


uint16 pec15 (char *data , int len)
{
        uint16 remainder,address;
        remainder = 16;//PEC seed
        int i;
        for (i = 0; i < len; i++)
        {
                address = ((remainder >> 7) ^ data[i]) & 0xff;//calculate PEC table address
                remainder = (remainder << 8 ) ^ pec15Table[address];
        }
        return (remainder*2);//The CRC15 has a 0 in the LSB so the final value must be multiplied by 2
}

void LTC6813_init_cfg(uint8 total_ic,
	LTC6813_cell_asic *ic)
{
	uint8 current_ic,j;
	for (current_ic = 0; current_ic<total_ic;current_ic++)  {
		for (j =0; j<6; j++)
		{
		  ic[current_ic].config.tx_data[j] = 0;
		}
	}
	for (current_ic = 0; current_ic<total_ic;current_ic++)  {
		for (j =0; j<6; j++)
		{
		  ic[current_ic].configb.tx_data[j] = 0;
		}
	}
}

//Helper function to set CFGR variable
void LTC6813_set_cfgr(uint8 nIC,
		LTC6813_cell_asic *ic,
		LTC6813_config config
					 )
{
	LTC6813_set_cfgr_refon(nIC,ic,config.REFON);
	LTC6813_set_cfgr_adcopt(nIC,ic,config.ADCOPT);
	LTC6813_set_cfgr_gpio(nIC,ic,config.gpio_pull_down);
	LTC6813_set_cfgr_dis(nIC,ic,config.dccBits);
	LTC6813_set_cfgr_dcto(nIC,ic,config.dctoBits);
	LTC6813_set_cfgr_uv(nIC, ic, config.UV);
	LTC6813_set_cfgr_ov(nIC, ic, config.OV);
    LTC6813_set_cfgrb_fdrf(nIC,ic,config.FDRF);
    LTC6813_set_cfgrb_dtmen(nIC,ic,config.DTMEN);
    LTC6813_set_cfgrb_ps(nIC,ic,config.ps);
}


//Helper function to set the REFON bit
void LTC6813_set_cfgr_refon(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_REFON refon)
{
	if (refon==REF_ON_ALWAYS) ic[nIC].config.tx_data[0] = ic[nIC].config.tx_data[0]|0x04;
	else ic[nIC].config.tx_data[0] = ic[nIC].config.tx_data[0]&0xFB;
}

//Helper function to set the adcopt bit
void LTC6813_set_cfgr_adcopt(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_ADCOPT adcopt)
{
	if (adcopt==ADCOPT_SLOW) ic[nIC].config.tx_data[0] = ic[nIC].config.tx_data[0]|0x01;
	else ic[nIC].config.tx_data[0] = ic[nIC].config.tx_data[0]&0xFE;
}

//Helper function to set GPIO bits
void LTC6813_set_cfgr_gpio(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_GPIO_PULL_DOWN gpio[9])
{
	uint8 i;
	for (i =0; i<5; i++)
	{
		if (gpio[i]==PULL_DOWN_OFF)ic[nIC].config.tx_data[0] = ic[nIC].config.tx_data[0]|(0x01<<(i+3));
		else ic[nIC].config.tx_data[0] = ic[nIC].config.tx_data[0]&(~(0x01<<(i+3)));
	}
	for (i =0; i<4; i++)
	{
	      if(gpio[i+5]==PULL_DOWN_OFF)ic[nIC].configb.tx_data[0] = ic[nIC].configb.tx_data[0]|(0x01<<i);
	      else ic[nIC].configb.tx_data[0] = ic[nIC].configb.tx_data[0]&(~(0x01<<i));
	}
}

//Helper function to control discharge
void LTC6813_set_cfgr_dis(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_DCC dcc[19])
{
	int i;
	for (i =0; i<8; i++)
	{
		if (dcc[i+1]==DISCHARGE_ENABLE)ic[nIC].config.tx_data[4] = ic[nIC].config.tx_data[4]|(0x01<<i);
		else ic[nIC].config.tx_data[4] = ic[nIC].config.tx_data[4]& (~(0x01<<i));
	}
	for (i =0; i<4; i++)
	{
		if (dcc[i+9]==DISCHARGE_ENABLE)ic[nIC].config.tx_data[5] = ic[nIC].config.tx_data[5]|(0x01<<i);
		else ic[nIC].config.tx_data[5] = ic[nIC].config.tx_data[5]&(~(0x01<<i));
	}

	for(i =0;i<7;i++)
	{
		if(i==0)
		{
			if(dcc[i]==DISCHARGE_ENABLE)ic[nIC].configb.tx_data[1] = ic[nIC].configb.tx_data[1]|0x04;
			else ic[nIC].configb.tx_data[1] = ic[nIC].configb.tx_data[1]&0xFB;
		}
		if((i>0) && (i<5))
		{
			if(dcc[i+12]==DISCHARGE_ENABLE)ic[nIC].configb.tx_data[0] = ic[nIC].configb.tx_data[0]|(0x01<<(i+3));
			else ic[nIC].configb.tx_data[0] = ic[nIC].configb.tx_data[0]&(~(0x01<<(i+3)));
		}
		if((i>4) && (i<7))
		{
			if(dcc[i+12]==DISCHARGE_ENABLE)ic[nIC].configb.tx_data[1] = ic[nIC].configb.tx_data[1]|(0x01<<(i-5));
			else ic[nIC].configb.tx_data[1] = ic[nIC].configb.tx_data[1]&(~(0x01<<(i-5)));
		}
	}
}

//Helper function to control discharge time value
void LTC6813_set_cfgr_dcto(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_DCTO dcto)
{
	uint8 i;
	ic[nIC].config.tx_data[5]=ic[nIC].config.tx_data[5]&(0x0f);
	ic[nIC].config.tx_data[5]=ic[nIC].config.tx_data[5]|((0x0f&dcto)<<4);
}
//Helper Function to set uv value in CFG register (uv set in 100uV step)
void LTC6813_set_cfgr_uv(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_VUV uv)
{
	LTC6813_VUV tmp = (uv/16)-1;
	ic[nIC].config.tx_data[1] = 0x00FF & tmp;
	ic[nIC].config.tx_data[2] = ic[nIC].config.tx_data[2]&0xF0;
	ic[nIC].config.tx_data[2] = ic[nIC].config.tx_data[2]|((0x0F00 & tmp)>>8);
}

//helper function to set OV value in CFG register (uv set in 100uV step)
void LTC6813_set_cfgr_ov(uint8 nIC, LTC6813_cell_asic *ic,LTC6813_VOV ov)
{
	LTC6813_VOV tmp = (ov/16);
	ic[nIC].config.tx_data[3] = 0x00FF & (tmp>>4);
	ic[nIC].config.tx_data[2] = ic[nIC].config.tx_data[2]&0x0F;
	ic[nIC].config.tx_data[2] = ic[nIC].config.tx_data[2]|((0x000F & tmp)<<4);
}

//Helper function to set the FDRF bit
void LTC6813_set_cfgrb_fdrf(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_FDRF fdrf)
{
	if(fdrf==FORCE_DR_FAIL) ic[nIC].configb.tx_data[1] = ic[nIC].configb.tx_data[1]|0x40;
	else ic[nIC].configb.tx_data[1] = ic[nIC].configb.tx_data[1]&0xBF;
}

//Helper function to set the DTMEN bit
void LTC6813_set_cfgrb_dtmen(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_DTMEN dtmen)
{
	if(dtmen==FORCE_DR_FAIL) ic[nIC].configb.tx_data[1] = ic[nIC].configb.tx_data[1]|0x08;
	else ic[nIC].configb.tx_data[1] = ic[nIC].configb.tx_data[1]&0xF7;
}

//Helper function to set the PATH SELECT bit
void LTC6813_set_cfgrb_ps(uint8 nIC, LTC6813_cell_asic *ic, LTC6813_PS ps)
{
	ic[nIC].configb.tx_data[1]=ic[nIC].configb.tx_data[1]&0xCF;
	ic[nIC].configb.tx_data[1]=ic[nIC].configb.tx_data[1]|((ps&0x03)<<4);
}

//Helper Function to reset PEC counters
void LTC681x_reset_crc_count(uint8 total_ic,
		LTC6813_cell_asic *ic)
{
	int current_ic,i;
  for (current_ic = 0 ; current_ic < total_ic; current_ic++)
  {
    ic[current_ic].crc_count.pec_count = 0;
    ic[current_ic].crc_count.cfgr_pec = 0;
    for (i=0; i<6; i++)
    {
      ic[current_ic].crc_count.cell_pec[i]=0;

    }
    for (i=0; i<4; i++)
    {
      ic[current_ic].crc_count.aux_pec[i]=0;
    }
    for (i=0; i<2; i++)
    {
      ic[current_ic].crc_count.stat_pec[i]=0;
    }
  }
}

//Helper function to intialize register limits.
void LTC6813_init_reg_limits(uint8 total_ic, LTC6813_cell_asic *ic)
{
	uint8 cic;
    for(cic=0; cic<total_ic; cic++)
    {
        ic[cic].ic_reg.cell_channels=18;
        ic[cic].ic_reg.stat_channels=4;
        ic[cic].ic_reg.aux_channels=9;
        ic[cic].ic_reg.num_cv_reg=6;
        ic[cic].ic_reg.num_gpio_reg=4;
        ic[cic].ic_reg.num_stat_reg=2;
    }
}

//Helper function that increments PEC counters
void LTC681x_check_pec(uint8 total_ic,
						PEC_TYPE reg,
						LTC6813_cell_asic *ic
					   )
{
	int current_ic,i;
  switch (reg)
  {
    case CFGR:
      for (current_ic = 0 ; current_ic < total_ic; current_ic++)
      {
        ic[current_ic].crc_count.pec_count = ic[current_ic].crc_count.pec_count + ic[current_ic].config.rx_pec_match;
        ic[current_ic].crc_count.cfgr_pec = ic[current_ic].crc_count.cfgr_pec + ic[current_ic].config.rx_pec_match;
      }
    break;

    case CFGRB:
      for (current_ic = 0 ; current_ic < total_ic; current_ic++)
      {
        ic[current_ic].crc_count.pec_count = ic[current_ic].crc_count.pec_count + ic[current_ic].configb.rx_pec_match;
        ic[current_ic].crc_count.cfgr_pec = ic[current_ic].crc_count.cfgr_pec + ic[current_ic].configb.rx_pec_match;
      }
    break;
    case CELL:
      for (current_ic = 0 ; current_ic < total_ic; current_ic++)
      {
        for (i=0; i<ic[0].ic_reg.num_cv_reg; i++)
        {
          ic[current_ic].crc_count.pec_count = ic[current_ic].crc_count.pec_count + ic[current_ic].cells.pec_match[i];
          ic[current_ic].crc_count.cell_pec[i] = ic[current_ic].crc_count.cell_pec[i] + ic[current_ic].cells.pec_match[i];
        }
      }
    break;
    case AUX:
      for (current_ic = 0 ; current_ic < total_ic; current_ic++)
      {
        for (i=0; i<ic[0].ic_reg.num_gpio_reg; i++)
        {
          ic[current_ic].crc_count.pec_count = ic[current_ic].crc_count.pec_count + (ic[current_ic].aux.pec_match[i]);
          ic[current_ic].crc_count.aux_pec[i] = ic[current_ic].crc_count.aux_pec[i] + (ic[current_ic].aux.pec_match[i]);
        }
      }

    break;
    case STAT:
      for (current_ic = 0 ; current_ic < total_ic; current_ic++)
      {

        for (i=0; i<ic[0].ic_reg.num_stat_reg-1; i++)
        {
          ic[current_ic].crc_count.pec_count = ic[current_ic].crc_count.pec_count + ic[current_ic].stat.pec_match[i];
          ic[current_ic].crc_count.stat_pec[i] = ic[current_ic].crc_count.stat_pec[i] + ic[current_ic].stat.pec_match[i];
        }
      }
    break;
    default:
    break;
  }
}

//Helper function that parses voltage measurement registers
sint8 parse_cells(uint8 current_ic,
					uint8 cell_reg,
					uint8 cell_data[],
					uint16 *cell_codes,
					uint8 *ic_pec
					)
{

  const uint8 BYT_IN_REG = 6;
  const uint8 CELL_IN_REG = 3;
  sint8 pec_error = 0;
  uint16 parsed_cell;
  uint16 received_pec;
  uint16 data_pec;
  uint8 data_counter = current_ic*NUM_RX_BYT; //data counter
  uint8 current_cell;

  for (current_cell = 0; current_cell<CELL_IN_REG; current_cell++)  // This loop parses the read back data into cell voltages, it
  {																			// loops once for each of the 3 cell voltage codes in the register


    parsed_cell = cell_data[data_counter] + (cell_data[data_counter + 1] << 8);//Each cell code is received as two bytes and is combined to
																			  // create the parsed cell voltage code
    cell_codes[current_cell  + ((cell_reg - 1) * CELL_IN_REG)] = parsed_cell;

    data_counter = data_counter + 2;                       //Because cell voltage codes are two bytes the data counter
														  //must increment by two for each parsed cell code
  }

  received_pec = (cell_data[data_counter] << 8) | cell_data[data_counter+1]; //The received PEC for the current_ic is transmitted as the 7th and 8th
																			//after the 6 cell voltage data bytes
  data_pec = pec15(&cell_data[(current_ic) * NUM_RX_BYT],BYT_IN_REG);

  if (received_pec != data_pec)
  {
    pec_error = 1;                             //The pec_error variable is simply set negative if any PEC errors
    ic_pec[cell_reg-1]=1;
  }
  else
  {
    ic_pec[cell_reg-1]=0;
  }
  data_counter=data_counter+2;
  return(pec_error);
}

