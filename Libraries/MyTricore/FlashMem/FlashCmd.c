/*
 * FlashCmd.c
 *
 *  Created on: Sep 24, 2021
 *      Author: weetit
 */


#include "FlashCmd.h"
#include "FlashMem.h"
#include "FlashDevice.h"
#include "string.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Include here

//Declaration here

//Global variable

SerialFlashFile g_SerialFlashFile;

//Code here

uint32 SerialFlashInit()
{
    uint32 ID;
    uint8 buf[TLF_BUFFER_SIZE];
    Flash_init();
    SerialFlashReadID(buf);
    ID=buf[0];
    return ID;
}

void SerialFlashReadID(uint8 *buf)
{
    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_Cpu.drivers.spiMasterFlashChannel) == SpiIf_Status_busy)  {};
    memset(g_Qspi_Cpu.qspiBuffer.spiTxBuffer,0,TLF_BUFFER_SIZE);

    g_Qspi_Cpu.qspiBuffer.spiTxBuffer[0]=SPIFLASH_READ_ID;

    IfxQspi_SpiMaster_exchange(&g_Qspi_Cpu.drivers.spiMasterFlashChannel, &g_Qspi_Cpu.qspiBuffer.spiTxBuffer[0],
            &g_Qspi_Cpu.qspiBuffer.spiRxBuffer[0], TLF_BUFFER_SIZE);

    memcpy(buf,g_Qspi_Cpu.qspiBuffer.spiRxBuffer,TLF_BUFFER_SIZE);
}

void SerialFlashRead(uint32 addr, void *buf, uint32 len)
{
}

void SerialFlashWrite(uint32 addr, const void *buf, uint32 len)
{
}
