/**
 * \file TLF35584.h
 * \brief
 *
 * \version V0.2
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Demo_TLF35584_SrcDoc_Main Demo Source
 * \ingroup IfxLld_Demo_TLF35584_SrcDoc
 * \defgroup IfxLld_Demo_TLF35584_SrcDoc_Main_Interrupt Interrupts
 * \ingroup IfxLld_Demo_TLF35584_SrcDoc_Main
 */

#ifndef LTC6813_H
#define LTC6813_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include <Cpu/Std/Ifx_Types.h>
#include <Qspi/SpiMaster/IfxQspi_SpiMaster.h>
#include "ConfigurationBMS.h"
#include "LTC6813_CMD.h"

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/


#define LTC6813_TOTAL_IC 1

/******************************************************************************/
/*--------------------------------Enumerations--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/


/******************************************************************************/
/*------------------------------Global variables------------------------------*/
/******************************************************************************/

IFX_EXTERN App_Qspi_Cpu g_Qspi_Cpu;

IFX_EXTERN LTC6813_cell_asic bms_ic[LTC6813_TOTAL_IC];
IFX_EXTERN LTC6813_config bms_config;

/******************************************************************************/
/*-------------------------Function Prototypes--------------------------------*/
/******************************************************************************/
IFX_EXTERN void LTC6813_read_write_init(void);
IFX_EXTERN uint32 LTC6813_read_write(uint32 send_data);
IFX_EXTERN uint32 *LTC6813_read_write_buffer(uint32 size);
IFX_EXTERN void LTC6813_wakeup(uint8 total_ic);
IFX_EXTERN void LTC6813_wrcfg(uint8 total_ic, //The number of ICs being written to
		LTC6813_cell_asic *ic);
IFX_EXTERN void LTC6813_wrcfgb(uint8 total_ic, //The number of ICs being written to
		LTC6813_cell_asic *ic);

IFX_EXTERN void LTC6813_adcv( LTC6813_MD MD, //ADC Mode
		LTC6813_DCP DCP, //Discharge Permit
		LTC6813_CELL_CH CH //Cell Channels to be measured
                 );
IFX_EXTERN uint8 LTC6813_rdcv(LTC6813_REG reg, // Controls which cell voltage register is read back.
                     uint8 total_ic, // the number of ICs in the system
                     LTC6813_cell_asic *ic // Array of the parsed cell codes
                    );
IFX_EXTERN void LTC6813_rdcv_reg(LTC6813_REG reg, //Determines which cell voltage register is read back
                      uint8 total_ic, //the number of ICs in the
                      uint8 *data //An array of the unparsed cell codes
                     );

IFX_EXTERN Ifx_TickTime LTC6813_pollAdc();
IFX_EXTERN sint8 LTC6813_rdcfg(uint8 total_ic, //Number of ICs in the system
		LTC6813_cell_asic *ic
                    );
IFX_EXTERN sint8 LTC6813_rdcfgb(uint8 total_ic, //Number of ICs in the system
		LTC6813_cell_asic *ic
                     );


IFX_EXTERN void LTC6813_cmd_68(uint8 tx_cmd[2]);
IFX_EXTERN void LTC6813_write_68(uint8 total_ic,
			  uint8 tx_cmd[2],
			  uint8 *data);
IFX_EXTERN sint8 LTC6813_read_68( uint8 total_ic,
				uint8 tx_cmd[2],
				uint8 *rx_data);

IFX_EXTERN void print_cell_voltage();


#endif  // LTC6813_H
