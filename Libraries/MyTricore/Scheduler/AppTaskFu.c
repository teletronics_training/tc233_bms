/*
 * AppTaskFu.c
 *
 *  Created on: 19.09.2013
 *
 */


#include "AppTaskFu.h"
#include "ConfigurationECU.h"
#include "LCD_SCREEN.h"
#include "LTC6813.h"
#include "AppBMSDriver.h"

typedef enum
{
    TEST_BOARD=0,
    TEST_RC,
    TEST_PWM,
    TEST_Relay,
    TEST_GPIO,
    TEST_Number
}Test_Senario_Type;

Test_Senario_Type senario=TEST_BOARD;


void appTaskfu_1ms(void)
{
//    transmitCanMessage();

}

void appTaskfu_10ms(void)
{

}

void appTaskfu_100ms(void)
{
    char string1[32],string2[32],string3[32],string4[32];
    char string_on[5] ="ON  ";
    char string_off[5]="OFF ";
    char string_NA[5]="N/A ";
//    sendCanMessage(0x12345678,0xABCDEF00);
//    getDutyCycle();

    static uint16 run_once=0;
    LCD_draw();

    switch(senario)
    {
        case TEST_BOARD:
        {
            // Initialize LCD Driver
            /* Initialize the VADC */
            sprintf(string1,"                     ");
            sprintf(string2,"                     ");
            sprintf(string3,"                     ");
            sprintf(string4,"                     ");

            if(g_AppBMS_data.totalV<90)
            {
                ftoa(g_AppBMS_data.totalV, &string1[0], 1);
                intToStr(g_AppBMS_data.discharge_cell, &string1[5], 1);
                ftoa(g_AppBMS_data.cell[0], &string1[8], 1);
                ftoa(g_AppBMS_data.cell[1], &string1[12], 1);
                ftoa(g_AppBMS_data.cell[2], &string1[16], 1);
                ftoa(g_AppBMS_data.cell[3], &string2[0], 1);
                ftoa(g_AppBMS_data.cell[4], &string2[4], 1);
                ftoa(g_AppBMS_data.cell[5], &string2[8], 1);
                ftoa(g_AppBMS_data.cell[6], &string2[12], 1);
                ftoa(g_AppBMS_data.cell[7], &string2[16], 1);
                ftoa(g_AppBMS_data.cell[8], &string3[0], 1);
                ftoa(g_AppBMS_data.cell[9], &string3[4], 1);
                ftoa(g_AppBMS_data.cell[10], &string3[8], 1);
                ftoa(g_AppBMS_data.cell[11], &string3[12], 1);
                ftoa(g_AppBMS_data.cell[12], &string3[16], 1);
                ftoa(g_AppBMS_data.cell[13], &string4[0], 1);
                ftoa(g_AppBMS_data.cell[14], &string4[4], 1);
                ftoa(g_AppBMS_data.cell[15], &string4[8], 1);
                ftoa(g_AppBMS_data.cell[16], &string4[12], 1);
                ftoa(g_AppBMS_data.cell[17], &string4[16], 1);
            }
            else
            {
                sprintf(string1,"Error                ");
            }



            uint8 buf[TLF_BUFFER_SIZE];

            break;
        }

//        case TEST_RC:
//        {
//            /* Initialize the VADC */
//            sprintf(string1,"Input capture ......");
//            sprintf(string2,"                    ");
//            sprintf(string3,"                    ");
//            sprintf(string4,"                    ");
//
//
//            ftoa(g_GtmTimPwm[0].g_measuredPwmDutyCycle, &string2[0], 2);
//            ftoa(g_GtmTimPwm[1].g_measuredPwmDutyCycle, &string2[6], 2);
//            ftoa(g_GtmTimPwm[2].g_measuredPwmDutyCycle, &string2[12], 2);
//            ftoa(g_GtmTimPwm[3].g_measuredPwmDutyCycle, &string3[0], 2);
//            ftoa(g_GtmTimPwm[4].g_measuredPwmDutyCycle, &string3[6], 2);
//            ftoa(g_GtmTimPwm[5].g_measuredPwmDutyCycle, &string3[12], 2);
//
//            ftoa(g_GtmTimPwm[0].g_measuredPwmPeriod, &string4[0], 4);
//            if(g_GtmTimPwm[0].g_dataCoherent)
//            {
//                string4[7]='T';
//            }
//            else
//            {
//                string4[7]='F';
//            }
//            break;
//        }
//        case TEST_PWM:
//        {
//            sprintf(string1,"PWM status .........");
//            sprintf(string2,"                    ");
//            sprintf(string3,"                    ");
//            sprintf(string4,"                    ");
//            PWM_GPIO_setDuty(PWM_GPIO_0,50);
//            PWM_GPIO_setDuty(PWM_GPIO_1,30);
//            PWM_GPIO_setDuty(PWM_GPIO_2,40);
//            PWM_GPIO_setDuty(PWM_GPIO_3,50);
//            PWM_GPIO_setDuty(PWM_GPIO_4,60);
//            break;
//        }
//        case TEST_Relay:
//        {
//            /* Initialize the VADC */
//            sprintf(string1,"Relay status .......");
//            sprintf(string2,"                    ");
//            sprintf(string3,"                    ");
//            sprintf(string4,"                    ");
//
//            int loop;
//            for(loop=0;loop<Relay_GPIO_Num;loop++)
//            {
//                if(loop<4)
//                {
//                    if(g_App_Relay_GPIO_OutPin[loop].port==NULL)
//                    {
//                        memcpy(&string2[loop*4],string_NA,4);
//                    }
//                    else if(g_App_Relay_GPIO_OutPin[loop].status)
//                    {
//                        memcpy(&string2[loop*4],string_on,4);
//                    }
//                    else
//                    {
//                        memcpy(&string2[loop*4],string_off,4);
//                    }
//                }
//                else if(loop<8)
//                {
//                    if(g_App_Relay_GPIO_OutPin[loop].port==NULL)
//                    {
//                        memcpy(&string3[(loop-4)*4],string_NA,4);
//                    }
//                    else if(g_App_Relay_GPIO_OutPin[loop].status)
//                    {
//                        memcpy(&string3[(loop-4)*4],string_on,4);
//                    }
//                    else
//                    {
//                        memcpy(&string3[(loop-4)*4],string_off,4);
//                    }
//                }
//            }
//
//
//            break;
//        }
//        case TEST_GPIO:
//        {
//            /* Initialize the VADC */
//            sprintf(string1,"GPIO status ........");
//            sprintf(string2,"                    ");
//            sprintf(string3,"                    ");
//            sprintf(string4,"                    ");
//
//            int loop;
//            for(loop=0;loop<GPIO_Num;loop++)
//            {
//                if(loop<4)
//                {
//                    if(g_App_GPIO[loop].port==NULL)
//                    {
//                        memcpy(&string2[loop*4],string_NA,4);
//                    }
//                    else if(g_App_GPIO[loop].status)
//                    {
//                        memcpy(&string2[loop*4],string_on,4);
//                    }
//                    else
//                    {
//                        memcpy(&string2[loop*4],string_off,4);
//                    }
//                }
//                else if(loop<8)
//                {
//                    if(g_App_GPIO[loop].port==NULL)
//                    {
//                        memcpy(&string3[(loop-4)*4],string_NA,4);
//                    }
//                    else if(g_App_GPIO[loop].status)
//                    {
//                        memcpy(&string3[(loop-4)*4],string_on,4);
//                    }
//                    else
//                    {
//                        memcpy(&string3[(loop-4)*4],string_off,4);
//                    }
//                }
//                else if(loop<12)
//                {
//                    if(g_App_GPIO[loop].port==NULL)
//                    {
//                        memcpy(&string4[(loop-8)*4],string_NA,4);
//                    }
//                    else if(g_App_GPIO[loop].status)
//                    {
//                        memcpy(&string4[(loop-8)*4],string_on,4);
//                    }
//                    else
//                    {
//                        memcpy(&string4[(loop-8)*4],string_off,4);
//                    }
//                }
//            }
//            break;
//        }
    }
//    g_AppBMS_data.discharge_cell=14;
    LCD_print(string1);
    LCD_print(string2);
    LCD_print(string3);
    LCD_print(string4);

    g_AppBMS_data.max_voltage=0.0;
    for(int i=0;i<18;i++)
    {
        if(g_AppBMS_data.cell[i]>g_AppBMS_data.max_voltage)
        {
            g_AppBMS_data.max_voltage=g_AppBMS_data.cell[i];
            if(g_AppBMS_data.max_voltage>4.3)
            {
                g_AppBMS_data.discharge_cell=i+1;
            }
            else
            {
                g_AppBMS_data.discharge_cell=0;
            }
        }
    }

}

void appTaskfu_1000ms(void)
{

    static int counter=0,test=0;
//  AppBMS_ConvertCellVoltage_Discharge(g_AppBMS_data.discharge_cell);
    AppBMS_ConvertCellVoltage();


//    {
//        senario=TEST_BOARD;
//        test++;
//    }
}

