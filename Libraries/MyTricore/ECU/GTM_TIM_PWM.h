/*
 * AppTimDriver.h
 *
 *  Created on: Oct 8, 2020
 *      Author: weeti
 */

#ifndef APPTIMDRIVER_H_
#define APPTIMDRIVER_H_


/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include <Ifx_Types.h>
#include "Configuration.h"
#include "SysSe/Bsp/Bsp.h"
#include "IfxGtm_Tim_In.h"
#include "IfxGtm_Tom_Pwm.h"

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/


typedef struct
{
    IfxGtm_Tim_In Tim;
    IfxGtm_Tim_TinMap *pwmIn;
    float32 g_measuredPwmDutyCycle;
    float32 g_measuredPwmFreq_Hz;
    float32 g_measuredPwmPeriod;
    boolean g_dataCoherent;
}App_GtmTimPwm;

IFX_EXTERN App_GtmTimPwm g_GtmTimPwm[6];

/******************************************************************************/
/*--------------------------------Enumerations--------------------------------*/
/******************************************************************************/


IFX_EXTERN void initGtmTimPwm(void);
IFX_EXTERN void getDutyCycle(void);

#endif /* APPTIMDRIVER_H_ */
