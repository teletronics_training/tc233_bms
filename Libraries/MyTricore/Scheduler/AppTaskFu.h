/*
 * AppTaskFu.h
 *
 *  Created on: 19.09.2013
 *
 */

#ifndef APPTASKFU_H_
#define APPTASKFU_H_


void appTaskfu_1ms(void);
void appTaskfu_10ms(void);
void appTaskfu_100ms(void);
void appTaskfu_1000ms(void);

#endif /* APPTASKFU_H_ */
