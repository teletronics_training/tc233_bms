/**
 * \file QspiTlfDemo.c
 * \brief
 *
 * \version V0.2
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 */

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "LTC6813.h"
#include <Qspi/SpiMaster/IfxQspi_SpiMaster.h>
#include "SysSe/Bsp/Bsp.h"
#include "MyBsp.h"
#include <stdlib.h>

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*--------------------------------Enumerations--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/


/******************************************************************************/
/*------------------------------Global variables------------------------------*/
/******************************************************************************/
/******************************************************************************/
/*-------------------------Function Prototypes--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*------------------------Private Variables/Constants-------------------------*/
/******************************************************************************/

LTC6813_cell_asic bms_ic[LTC6813_TOTAL_IC];
LTC6813_config bms_config;

//Global variable
App_Qspi_Cpu g_Qspi_Cpu;

/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/
/** \brief TLF (QSPI) initialization
 *
 * This function initializes Qspix in master mode.
 */


#if defined(AurixBoard_TC265)
IFX_INTERRUPT(qspi3TxISR, 0, ISR_PRIORITY_QSPI3_TX);
IFX_INTERRUPT(qspi3RxISR, 0, ISR_PRIORITY_QSPI3_RX);
IFX_INTERRUPT(qspi3ErISR, 0, ISR_PRIORITY_QSPI3_ER);
#elif defined(AurixBoard_TC233)
IFX_INTERRUPT(qspi0TxISR, 0, ISR_PRIORITY_QSPI0_TX);
IFX_INTERRUPT(qspi0RxISR, 0, ISR_PRIORITY_QSPI0_RX);
IFX_INTERRUPT(qspi0ErISR, 0, ISR_PRIORITY_QSPI0_ER);
#else
#error AurixBoard selection required.
#endif




void LTC6813_init(void)
{
    boolean interruptState = IfxCpu_disableInterrupts();

    // create module config
    IfxQspi_SpiMaster_Config spiMasterConfig;
#if defined(AurixBoard_TC265)
    IfxQspi_SpiMaster_initModuleConfig(&spiMasterConfig, &MODULE_QSPI3);
#elif defined(AurixBoard_TC233)
    IfxQspi_SpiMaster_initModuleConfig(&spiMasterConfig, &MODULE_QSPI0);
#else
#error AurixBoard selection required.
#endif

    // set the desired mode and maximum baudrate
    spiMasterConfig.base.mode             = SpiIf_Mode_master;
#if defined(AurixBoard_TC265)
    spiMasterConfig.base.maximumBaudrate  = QSPI3_MAX_BAUDRATE;
#elif defined(AurixBoard_TC233)
    spiMasterConfig.base.maximumBaudrate  = QSPI0_MAX_BAUDRATE;
#else
#error AurixBoard selection required.
#endif


    // ISR priorities and interrupt target

#if defined(AurixBoard_TC265)
    spiMasterConfig.base.txPriority       = ISR_PRIORITY_QSPI3_TX;
    spiMasterConfig.base.rxPriority       = ISR_PRIORITY_QSPI3_RX;
    spiMasterConfig.base.erPriority       = ISR_PRIORITY_QSPI3_ER;
    spiMasterConfig.base.isrProvider      = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());
#elif defined(AurixBoard_TC233)
    spiMasterConfig.base.txPriority       = ISR_PRIORITY_QSPI0_TX;
    spiMasterConfig.base.rxPriority       = ISR_PRIORITY_QSPI0_RX;
    spiMasterConfig.base.erPriority       = ISR_PRIORITY_QSPI0_ER;
    spiMasterConfig.base.isrProvider      = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());
#else
#error AurixBoard selection required.
#endif

    // pin configuration
#if defined(AurixBoard_TC265)
    const IfxQspi_SpiMaster_Pins pins = {
        &QSPI3_SCLK_PIN, IfxPort_OutputMode_pushPull, // SCLK
        &QSPI3_MTSR_PIN, IfxPort_OutputMode_pushPull, // MTSR
        &QSPI3_MRST_PIN, IfxPort_InputMode_pullDown,  // MRST
        IfxPort_PadDriver_cmosAutomotiveSpeed3 // pad driver mode
    };
#elif defined(AurixBoard_TC233)
    const IfxQspi_SpiMaster_Pins pins = {
        &QSPI0_SCLK_PIN, IfxPort_OutputMode_pushPull, // SCLK
        &QSPI0_MTSR_PIN, IfxPort_OutputMode_pushPull, // MTSR
        &QSPI0_MRST_PIN, IfxPort_InputMode_pullDown,  // MRST
        IfxPort_PadDriver_cmosAutomotiveSpeed3 // pad driver mode
    };
#else
#error AurixBoard selection required.
#endif


    spiMasterConfig.pins = &pins;

    // initialize module
    IfxQspi_SpiMaster spi; // defined globally
    IfxQspi_SpiMaster_initModule(&g_Qspi_Cpu.drivers.spiMaster, &spiMasterConfig);




    // create channel config
    IfxQspi_SpiMaster_ChannelConfig spiMasterChannelConfig;
    IfxQspi_SpiMaster_initChannelConfig(&spiMasterChannelConfig, &g_Qspi_Cpu.drivers.spiMaster);

    // set the baudrate for this channel
    spiMasterChannelConfig.base.baudrate = 500000;
    spiMasterChannelConfig.base.mode.dataWidth=8;
    spiMasterChannelConfig.base.mode.csTrailDelay = 3;
    spiMasterChannelConfig.base.mode.csLeadDelay = 3;
    spiMasterChannelConfig.base.mode.clockPolarity = SpiIf_ClockPolarity_idleLow;
    spiMasterChannelConfig.base.mode.dataHeading = 1;
    spiMasterChannelConfig.base.mode.csInactiveDelay = 2;
    spiMasterChannelConfig.base.mode.shiftClock = SpiIf_ShiftClock_shiftTransmitDataOnTrailingEdge;


    // select pin configuration
    const IfxQspi_SpiMaster_Output slsOutput = {
        &LTC6813_USE_CHIPSELECT,
        IfxPort_OutputMode_pushPull,
        IfxPort_PadDriver_cmosAutomotiveSpeed1
    };
    spiMasterChannelConfig.sls.output = slsOutput;

    // initialize channel
    IfxQspi_SpiMaster_initChannel(&g_Qspi_Cpu.drivers.spiMasterLTC6813Channel, &spiMasterChannelConfig);
    IfxCpu_restoreInterrupts(interruptState);
    initTime();
	Ifx_TickTime time;

	time = getDeadLine(TimeConst_1ms*15);
    while(isDeadLine(time) != TRUE);

    init_PEC15_Table();

    g_Qspi_Cpu.qspiBuffer.txData=g_Qspi_Cpu.qspiBuffer.spiTxBuffer;
    g_Qspi_Cpu.qspiBuffer.rxData=g_Qspi_Cpu.qspiBuffer.spiRxBuffer;
}

void print_cell_voltage()
{
	char str[32];
	int i;

    LTC6813_init_cfg(LTC6813_TOTAL_IC,bms_ic);
    LTC6813_init_reg_limits(LTC6813_TOTAL_IC, bms_ic);


    bms_config.REFON=REF_ON_ALWAYS;
    bms_config.ADCOPT=ADCOPT_FAST;
    bms_config.gpio_pull_down[0]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[1]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[2]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[3]=PULL_DOWN_ON;
    bms_config.gpio_pull_down[4]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[5]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[6]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[7]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[8]=PULL_DOWN_OFF;
    bms_config.dccBits[0]=DISCHARGE_DISABLE;
    bms_config.dccBits[1]=DISCHARGE_DISABLE;
    bms_config.dccBits[2]=DISCHARGE_DISABLE;
    bms_config.dccBits[3]=DISCHARGE_DISABLE;
    bms_config.dccBits[4]=DISCHARGE_DISABLE;
    bms_config.dccBits[5]=DISCHARGE_DISABLE;
    bms_config.dccBits[6]=DISCHARGE_DISABLE;
	bms_config.dccBits[7]=DISCHARGE_DISABLE;
	bms_config.dccBits[8]=DISCHARGE_DISABLE;
	bms_config.dccBits[9]=DISCHARGE_DISABLE;
	bms_config.dccBits[10]=DISCHARGE_DISABLE;
	bms_config.dccBits[11]=DISCHARGE_DISABLE;
    bms_config.dccBits[12]=DISCHARGE_DISABLE;
    bms_config.dccBits[13]=DISCHARGE_DISABLE;
    bms_config.dccBits[14]=DISCHARGE_DISABLE;
    bms_config.dccBits[15]=DISCHARGE_DISABLE;
    bms_config.dccBits[16]=DISCHARGE_DISABLE;
    bms_config.dccBits[17]=DISCHARGE_DISABLE;
    bms_config.dccBits[18]=DISCHARGE_DISABLE;
    bms_config.dctoBits=DCTO_DISABLE;
    bms_config.UV=33000;
    bms_config.OV=41000;
    bms_config.FDRF=NORMAL_DR;
    bms_config.DTMEN=DTM_DISABLE;
    bms_config.ps=PS_ALL;

    for(i=0;i<LTC6813_TOTAL_IC;i++)
    {
    	LTC6813_set_cfgr(i,bms_ic,bms_config);
    }

    //LTC6813_reset_crc_count(1,bms_ic);
    LTC6813_init_reg_limits(LTC6813_TOTAL_IC,bms_ic);
    LTC6813_wakeup(LTC6813_TOTAL_IC);
    LTC6813_wrcfg(LTC6813_TOTAL_IC,bms_ic);

    LTC6813_wrcfgb(LTC6813_TOTAL_IC,bms_ic);

    LTC6813_rdcfg(LTC6813_TOTAL_IC,bms_ic);

    LTC6813_rdcfgb(LTC6813_TOTAL_IC,bms_ic);

    LTC6813_adcv( MD_7KHZ_3KHZ, //ADC Mode
    		DCP_DISABLED, //Discharge Permit
    		CELL_CH_ALL //Cell Channels to be measured
                     );


    uint32 conv_time=LTC6813_pollAdc();


    LTC6813_rdcv(REG_ALL,LTC6813_TOTAL_IC,bms_ic);

    clear_display();
    //string2lcd("Cell conversion:");

    home_line1();
    sprintf(str,"%1.1f %1.1f %1.1f %1.1f %1.1f",
    		bms_ic[0].cells.c_codes[0]*0.0001,
    		bms_ic[0].cells.c_codes[1]*0.0001,
    		bms_ic[0].cells.c_codes[2]*0.0001,
    		bms_ic[0].cells.c_codes[3]*0.0001,
    		bms_ic[0].cells.c_codes[4]*0.0001);
    string2lcd(str);
    home_line2();
    sprintf(str,"%1.1f %1.1f %1.1f %1.1f %1.1f",
    		bms_ic[0].cells.c_codes[5]*0.0001,
    		bms_ic[0].cells.c_codes[6]*0.0001,
    		bms_ic[0].cells.c_codes[7]*0.0001,
    		bms_ic[0].cells.c_codes[8]*0.0001,
    		bms_ic[0].cells.c_codes[9]*0.0001);
    string2lcd(str);
    home_line3();
    sprintf(str,"%1.1f %1.1f %1.1f %1.1f %1.1f",
    		bms_ic[0].cells.c_codes[10]*0.0001,
    		bms_ic[0].cells.c_codes[11]*0.0001,
    		bms_ic[0].cells.c_codes[12]*0.0001,
    		bms_ic[0].cells.c_codes[13]*0.0001,
    		bms_ic[0].cells.c_codes[14]*0.0001);
    string2lcd(str);
    home_line4();
    sprintf(str,"%1.1f %1.1f %1.1f",
    		bms_ic[0].cells.c_codes[15]*0.0001,
    		bms_ic[0].cells.c_codes[16]*0.0001,
    		bms_ic[0].cells.c_codes[17]*0.0001);
    string2lcd(str);
}

void LTC6813_wakeup(uint8 total_ic)
{
	int i;
	Ifx_TickTime time;
	for(i=0;i<total_ic;i++)
	{
	    Ifx_P *port=g_Qspi_Cpu.drivers.spiMasterLTC6813Channel.slso.port;
	    uint8  pinIndex=g_Qspi_Cpu.drivers.spiMasterLTC6813Channel.slso.pinIndex;
	    boolean status=__getbit(&port->OUT.U, pinIndex);
	    IfxPort_setPinMode(port, pinIndex, IfxPort_Mode_outputPushPullGeneral);
	    if(status)
	    {
            IfxPort_setPinState(port, pinIndex, IfxPort_State_low);
            time = getDeadLine(TimeConst_100us*3.5);
            while(isDeadLine(time) != TRUE);
	        IfxPort_setPinState(port, pinIndex, IfxPort_State_high);
	    }
	    else
	    {
	        IfxPort_setPinState(port, pinIndex, IfxPort_State_high);
            time = getDeadLine(TimeConst_100us*3.5);
            while(isDeadLine(time) != TRUE);
            IfxPort_setPinState(port, pinIndex, IfxPort_State_low);
	    }
        time = getDeadLine(TimeConst_10us*5);
        while(isDeadLine(time) != TRUE);
        IfxPort_setPinModeOutput(port, pinIndex, IfxPort_OutputMode_pushPull, IfxPort_OutputIdx_alt3);
        IfxPort_setPinPadDriver(port, pinIndex, IfxPort_PadDriver_cmosAutomotiveSpeed1);
	}
}


uint32 *LTC6813_read_write_buffer(uint32 size)
{
	char str[128];
    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_Cpu.drivers.spiMasterLTC6813Channel) == SpiIf_Status_busy)  {};

    IfxQspi_SpiMaster_exchange(&g_Qspi_Cpu.drivers.spiMasterLTC6813Channel, g_Qspi_Cpu.qspiBuffer.spiTxBuffer,
            g_Qspi_Cpu.qspiBuffer.spiRxBuffer, size);

    /* we wait until our values are read from Qspi */
    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_Cpu.drivers.spiMasterLTC6813Channel) == SpiIf_Status_busy)  {};

    return (&g_Qspi_Cpu.qspiBuffer.spiRxBuffer[0]);

}

uint32 LTC6813_read_write(uint32 send_data)
{

    g_Qspi_Cpu.qspiBuffer.txData[0] = send_data;

    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_Cpu.drivers.spiMasterLTC6813Channel) == SpiIf_Status_busy)  {};

    IfxQspi_SpiMaster_exchange(&g_Qspi_Cpu.drivers.spiMasterLTC6813Channel, &g_Qspi_Cpu.qspiBuffer.spiTxBuffer[0],
        &g_Qspi_Cpu.qspiBuffer.spiRxBuffer[0], 1);

    /* we wait until our values are read from Qspi */
    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_Cpu.drivers.spiMasterLTC6813Channel) == SpiIf_Status_busy)  {};



    return (g_Qspi_Cpu.qspiBuffer.spiRxBuffer[0]);

}

//Write the LTC6813 CFGRA
void LTC6813_wrcfg(uint8 total_ic, //The number of ICs being written to
		LTC6813_cell_asic *ic)
{
	uint8 cmd[2] = {(0xff&(WRCFGA>>8)) , (0xff&WRCFGA)} ;
	uint8 write_buffer[256];
	uint8 write_count = 0;
	uint8 c_ic = 0;
	uint8 current_ic;
	uint8 data;
  for (current_ic = 0; current_ic<total_ic; current_ic++)
  {
    if (ic->isospi_reverse == TRUE)
    {
      c_ic = current_ic;
    }
    else
    {
      c_ic = total_ic - current_ic - 1;
    }

    for (data = 0; data<6; data++)
    {
      write_buffer[write_count] = ic[c_ic].config.tx_data[data];
      write_count++;
    }
  }
  LTC6813_write_68(total_ic, cmd, write_buffer);
}

//Write the LTC6813 CFGRB
void LTC6813_wrcfgb(uint8 total_ic, //The number of ICs being written to
		LTC6813_cell_asic *ic)
{
	uint8 cmd[2] = {(0xff&(WRCFGB>>8)) , (0xff&WRCFGB)} ;
	uint8 write_buffer[256];
	uint8 write_count = 0;
	uint8 c_ic = 0;
	uint8 current_ic;
	uint8 data;
  for (current_ic = 0; current_ic<total_ic; current_ic++)
  {
    if (ic->isospi_reverse == TRUE)
    {
      c_ic = current_ic;
    }
    else
    {
      c_ic = total_ic - current_ic - 1;
    }

    for (data = 0; data<6; data++)
    {
      write_buffer[write_count] = ic[c_ic].configb.tx_data[data];
      write_count++;
    }
  }
  LTC6813_write_68(total_ic, cmd, write_buffer);
}

//Read CFGA
sint8 LTC6813_rdcfg(uint8 total_ic, //Number of ICs in the system
		LTC6813_cell_asic *ic
                    )
{
  uint8 cmd[2]= {(0xff&(RDCFGA>>8)) , (0xff&RDCFGA)} ;
  uint8 read_buffer[256];
  uint8 pec_error = 0;
  uint16 data_pec;
  uint16 calc_pec;
  uint8 c_ic = 0;
  uint8 current_ic;
  int byte;
  pec_error = LTC6813_read_68(total_ic, cmd, read_buffer);
  for (current_ic = 0; current_ic<total_ic; current_ic++)
  {
    if (ic->isospi_reverse == FALSE)
    {
      c_ic = current_ic;
    }
    else
    {
      c_ic = total_ic - current_ic - 1;
    }

    for (byte=0; byte<8; byte++)
    {
      ic[c_ic].config.rx_data[byte] = read_buffer[byte+(8*current_ic)];
    }
    calc_pec = pec15(&read_buffer[8*current_ic],6);
    data_pec = read_buffer[7+(8*current_ic)] | (read_buffer[6+(8*current_ic)]<<8);
    if (calc_pec != data_pec )
    {
      ic[c_ic].config.rx_pec_match = 1;
    }
    else ic[c_ic].config.rx_pec_match = 0;
  }
  LTC681x_check_pec(total_ic,CFGR,ic);
  return(pec_error);
}

//Reads CFGB
sint8 LTC6813_rdcfgb(uint8 total_ic, //Number of ICs in the system
		LTC6813_cell_asic *ic
                     )
{
  uint8 cmd[2]= {(0xff&(RDCFGB>>8)) , (0xff&RDCFGB)} ;
  uint8 read_buffer[256];
  uint8 pec_error = 0;
  uint16 data_pec;
  uint16 calc_pec;
  uint8 c_ic = 0;
  uint8 current_ic;
  int byte;
  pec_error = LTC6813_read_68(total_ic, cmd, read_buffer);
  for (current_ic = 0; current_ic<total_ic; current_ic++)
  {
    if (ic->isospi_reverse == FALSE)
    {
      c_ic = current_ic;
    }
    else
    {
      c_ic = total_ic - current_ic - 1;
    }

    for (byte=0; byte<8; byte++)
    {
      ic[c_ic].configb.rx_data[byte] = read_buffer[byte+(8*current_ic)];
    }
    calc_pec = pec15(&read_buffer[8*current_ic],6);
    data_pec = read_buffer[7+(8*current_ic)] | (read_buffer[6+(8*current_ic)]<<8);
    if (calc_pec != data_pec )
    {
      ic[c_ic].configb.rx_pec_match = 1;
    }
    else ic[c_ic].configb.rx_pec_match = 0;
  }
  LTC681x_check_pec(total_ic,CFGRB,ic);
  return(pec_error);
}

//Starts cell voltage conversion
void LTC6813_adcv( LTC6813_MD MD, //ADC Mode
		LTC6813_DCP DCP, //Discharge Permit
		LTC6813_CELL_CH CH //Cell Channels to be measured
                 )
{
	uint8 cmd[2];
	uint8 md_bits;
	md_bits = (MD & 0x02) >> 1;
	cmd[0] = md_bits + 0x02;
	md_bits = (MD & 0x01) << 7;
	cmd[1] =  md_bits + 0x60 + (DCP<<4) + CH;
	LTC6813_cmd_68(cmd);
}

// Reads the raw cell voltage register data
void LTC6813_rdcv_reg(LTC6813_REG reg, //Determines which cell voltage register is read back
                      uint8 total_ic, //the number of ICs in the
                      uint8 *data //An array of the unparsed cell codes
                     )
{
  const uint8 REG_LEN = 8; //number of bytes in each ICs register + 2 bytes for the PEC
  uint16 cmd_pec;
	uint8 current_ic;
	uint8 current_byte;
	int i;

	for (current_ic = 0; current_ic < total_ic; current_ic++) //executes for each LTC681x in the daisy chain and packs the data
	{																//into the r_comm array as well as check the received Config data for any bit errors
		for (current_byte = 0; current_byte < REG_LEN; current_byte++)
		{
		    g_Qspi_Cpu.qspiBuffer.txData[(current_ic*8)+current_byte+4] = 0x00;
		}
	}

  if (reg == REG_1)     //1: RDCVA
  {
      g_Qspi_Cpu.qspiBuffer.txData[1] = 0x04;
      g_Qspi_Cpu.qspiBuffer.txData[0] = 0x00;
  }
  else if (reg == REG_2) //2: RDCVB
  {
      g_Qspi_Cpu.qspiBuffer.txData[1] = 0x06;
      g_Qspi_Cpu.qspiBuffer.txData[0] = 0x00;
  }
  else if (reg == REG_3) //3: RDCVC
  {
      g_Qspi_Cpu.qspiBuffer.txData[1] = 0x08;
      g_Qspi_Cpu.qspiBuffer.txData[0] = 0x00;
  }
  else if (reg == REG_4) //4: RDCVD
  {
      g_Qspi_Cpu.qspiBuffer.txData[1] = 0x0A;
      g_Qspi_Cpu.qspiBuffer.txData[0] = 0x00;
  }
  else if (reg == REG_5) //4: RDCVE
  {
      g_Qspi_Cpu.qspiBuffer.txData[1] = 0x09;
      g_Qspi_Cpu.qspiBuffer.txData[0] = 0x00;
  }
  else if (reg == REG_6) //4: RDCVF
  {
      g_Qspi_Cpu.qspiBuffer.txData[1] = 0x0B;
      g_Qspi_Cpu.qspiBuffer.txData[0] = 0x00;
  }


  cmd_pec = pec15(g_Qspi_Cpu.qspiBuffer.txData,2);
  g_Qspi_Cpu.qspiBuffer.txData[2] = (uint8)(cmd_pec >> 8);
  g_Qspi_Cpu.qspiBuffer.txData[3] = (uint8)(cmd_pec);

  LTC6813_read_write_buffer(REG_LEN*total_ic+4);
  for(i=0;i<REG_LEN*total_ic;i++)
  {
	  data[i]=g_Qspi_Cpu.qspiBuffer.rxData[i+4];
  }
}


//Reads and parses the LTC681x cell voltage registers.
uint8 LTC6813_rdcv(LTC6813_REG reg, // Controls which cell voltage register is read back.
                     uint8 total_ic, // the number of ICs in the system
                     LTC6813_cell_asic *ic // Array of the parsed cell codes
                    )
{
  sint8 pec_error = 0;
  uint8 *cell_data;
  uint8 c_ic = 0;
  uint8 cell_reg;
  int current_ic;
  cell_data = (uint8 *) malloc(((NUM_RX_BYT+4)*total_ic)*sizeof(uint8));
  if(cell_data==NULL)
  {
      return 0;
  }


  if (reg == REG_ALL)
  {
    for (cell_reg = 1; cell_reg<ic[0].ic_reg.num_cv_reg+1; cell_reg++) //executes once for each of the LTC6811 cell voltage registers
    {
      LTC6813_rdcv_reg(cell_reg, total_ic,cell_data );
      for (current_ic = 0; current_ic<total_ic; current_ic++)
      {
        if (ic->isospi_reverse == FALSE)
        {
          c_ic = current_ic;
        }
        else
        {
          c_ic = total_ic - current_ic - 1;
        }
        pec_error = pec_error + parse_cells(current_ic,cell_reg, cell_data,
                                            &ic[c_ic].cells.c_codes[0],
                                            &ic[c_ic].cells.pec_match[0]);
      }
    }
   }

  else
  {
    LTC6813_rdcv_reg(reg, total_ic,cell_data);

    for (current_ic = 0; current_ic<total_ic; current_ic++)
    {
      if (ic->isospi_reverse == FALSE)
      {
        c_ic = current_ic;
      }
      else
      {
        c_ic = total_ic - current_ic - 1;
      }
      pec_error = pec_error + parse_cells(current_ic,reg, &cell_data[8*c_ic],
                                          &ic[c_ic].cells.c_codes[0],
                                          &ic[c_ic].cells.pec_match[0]);
    }
  }
  LTC681x_check_pec(total_ic,CELL,ic);
  free(cell_data);
  return(pec_error);
}



//Generic function to write 68xx commands. Function calculated PEC for tx_cmd data
void LTC6813_cmd_68(uint8 tx_cmd[2])
{
	uint8 cmd[4];
	uint16 cmd_pec;
	uint8 md_bits;
	g_Qspi_Cpu.qspiBuffer.txData[0]=tx_cmd[0];
	g_Qspi_Cpu.qspiBuffer.txData[1]=tx_cmd[1];
	cmd_pec = pec15(g_Qspi_Cpu.qspiBuffer.txData,2);
	g_Qspi_Cpu.qspiBuffer.txData[2]=(uint8)(cmd_pec >> 8);
	g_Qspi_Cpu.qspiBuffer.txData[3]=(uint8)(cmd_pec);
	LTC6813_read_write_buffer(4);
}

//Generic function to write 68xx commands and write payload data. Function calculated PEC for tx_cmd data
void LTC6813_write_68(uint8 total_ic,
			  uint8 tx_cmd[2],
			  uint8 *data)
{
	const uint8 BYTES_IN_REG = 6;
	const uint8 CMD_LEN = 4+(8*total_ic);
	uint16 data_pec;
	uint16 cmd_pec;
	uint8 cmd_index;
	uint8 current_ic;
	uint8 current_byte;

	for (current_ic = 0; current_ic < total_ic; current_ic++) //executes for each LTC681x in the daisy chain and packs the data
	{																//into the r_comm array as well as check the received Config data for any bit errors
		for (current_byte = 0; current_byte < BYTES_IN_REG; current_byte++)
		{
		    g_Qspi_Cpu.qspiBuffer.txData[(current_ic*8)+current_byte+4] = 0x00;
		}
	}

	g_Qspi_Cpu.qspiBuffer.txData[0] = tx_cmd[0];
	g_Qspi_Cpu.qspiBuffer.txData[1] = tx_cmd[1];
	cmd_pec = pec15(g_Qspi_Cpu.qspiBuffer.txData,2);
	g_Qspi_Cpu.qspiBuffer.txData[2] = (uint8)(cmd_pec >> 8);
	g_Qspi_Cpu.qspiBuffer.txData[3] = (uint8)(cmd_pec);
	cmd_index = 4;

	for (current_ic = total_ic; current_ic > 0; current_ic--)       // executes for each LTC681x, this loops starts with the last IC on the stack.
    {	                                                                   //The first configuration written is received by the last IC in the daisy chain
		for (current_byte = 0; current_byte < BYTES_IN_REG; current_byte++)
		{
		    g_Qspi_Cpu.qspiBuffer.txData[cmd_index] = data[((current_ic-1)*6)+current_byte];
			cmd_index = cmd_index + 1;
		}
		data_pec = (uint16)pec15(&g_Qspi_Cpu.qspiBuffer.txData[(current_ic-1)*6+4],BYTES_IN_REG);    // calculating the PEC for each Iss configuration register data
		g_Qspi_Cpu.qspiBuffer.txData[cmd_index] = (uint8)(data_pec >> 8);
		g_Qspi_Cpu.qspiBuffer.txData[cmd_index + 1] = (uint8)data_pec;
		cmd_index = cmd_index + 2;
	}
	LTC6813_read_write_buffer(CMD_LEN);
}

//Generic function to write 68xx commands and read data. Function calculated PEC for tx_cmd data
sint8 LTC6813_read_68( uint8 total_ic,
				uint8 tx_cmd[2],
				uint8 *rx_data)
{
	const uint8 BYTES_IN_REG = 8;
	uint8 data[256];
	int pec_error = 0;
	uint16 cmd_pec;
	uint16 data_pec;
	uint16 received_pec;
	uint8 current_ic,current_byte;

	for (current_ic = 0; current_ic < total_ic; current_ic++) //executes for each LTC681x in the daisy chain and packs the data
	{																//into the r_comm array as well as check the received Config data for any bit errors
		for (current_byte = 0; current_byte < BYTES_IN_REG; current_byte++)
		{
		    g_Qspi_Cpu.qspiBuffer.txData[(current_ic*8)+current_byte] = 0x00;
		}
	}

	g_Qspi_Cpu.qspiBuffer.txData[0] = tx_cmd[0];
	g_Qspi_Cpu.qspiBuffer.txData[1] = tx_cmd[1];
	cmd_pec = pec15(g_Qspi_Cpu.qspiBuffer.txData,2);
	g_Qspi_Cpu.qspiBuffer.txData[2] = (uint8)(cmd_pec >> 8);
	g_Qspi_Cpu.qspiBuffer.txData[3] = (uint8)(cmd_pec);

	LTC6813_read_write_buffer(BYTES_IN_REG*total_ic+4);         //Read the configuration data of all ICs on the daisy chain into rx_data[] array

	for (current_ic = 0; current_ic < total_ic; current_ic++) //executes for each LTC681x in the daisy chain and packs the data
	{																//into the r_comm array as well as check the received Config data for any bit errors
		for (current_byte = 0; current_byte < BYTES_IN_REG; current_byte++)
		{
			rx_data[(current_ic*8)+current_byte] = g_Qspi_Cpu.qspiBuffer.rxData[current_byte + (current_ic*BYTES_IN_REG)+4];
		}
		received_pec = (rx_data[(current_ic*8)+6]<<8) + rx_data[(current_ic*8)+7];
		data_pec = pec15(&rx_data[current_ic*8],6);
		if (received_pec != data_pec)
		{
		  pec_error = -1;
		}
	}
	return(pec_error);
}

//This function will block operation until the ADC has finished it's conversion
Ifx_TickTime LTC6813_pollAdc()
{
  uint32 counter = 0;
  uint8 finished = 0;
  uint8 current_time = 0;
  uint8 cmd[4];
  uint16 cmd_pec;

  Ifx_TickTime time=now();

  IfxPort_setPinState(LTC6813_USE_CHIPSELECT.pin.port, LTC6813_USE_CHIPSELECT.pin.pinIndex, IfxPort_State_high);
  IfxPort_setPinModeOutput(LTC6813_USE_CHIPSELECT.pin.port, LTC6813_USE_CHIPSELECT.pin.pinIndex, IfxPort_OutputMode_pushPull, IfxPort_OutputIdx_general);
  IfxPort_setPinPadDriver(LTC6813_USE_CHIPSELECT.pin.port, LTC6813_USE_CHIPSELECT.pin.pinIndex, IfxPort_PadDriver_cmosAutomotiveSpeed1);

  g_Qspi_Cpu.qspiBuffer.txData[0] = 0x07;
  g_Qspi_Cpu.qspiBuffer.txData[1] = 0x14;
  cmd_pec = pec15(g_Qspi_Cpu.qspiBuffer.txData,2);
  g_Qspi_Cpu.qspiBuffer.txData[2] = (uint8)(cmd_pec >> 8);
  g_Qspi_Cpu.qspiBuffer.txData[3] = (uint8)(cmd_pec);

  IfxPort_setPinState(LTC6813_USE_CHIPSELECT.pin.port, LTC6813_USE_CHIPSELECT.pin.pinIndex, IfxPort_State_low);
  LTC6813_read_write_buffer(4);

  /* we wait until our values are read from Qspi */
  while ((counter<20000))
  {
	  counter = counter + 1;
	  if(IfxPort_getPinState(&MODULE_P22, 2)!=IfxPort_State_high)
		  break;
  }
  IfxPort_setPinState(LTC6813_USE_CHIPSELECT.pin.port, LTC6813_USE_CHIPSELECT.pin.pinIndex, IfxPort_State_high);
  IfxPort_setPinModeOutput(LTC6813_USE_CHIPSELECT.pin.port, LTC6813_USE_CHIPSELECT.pin.pinIndex, IfxPort_OutputMode_pushPull, LTC6813_USE_CHIPSELECT.select);
  IfxPort_setPinPadDriver(LTC6813_USE_CHIPSELECT.pin.port, LTC6813_USE_CHIPSELECT.pin.pinIndex, IfxPort_PadDriver_cmosAutomotiveSpeed1);

  return(now()-time);
}



#if defined(AurixBoard_TC265)

IFX_INTERRUPT(qspi3TxISR, 0, ISR_PRIORITY_QSPI3_TX)
{
    IfxQspi_SpiMaster_isrTransmit(&g_Qspi_Cpu.drivers.spiMaster);
}

IFX_INTERRUPT(qspi3RxISR, 0, ISR_PRIORITY_QSPI3_RX)
{
    IfxQspi_SpiMaster_isrReceive(&g_Qspi_Cpu.drivers.spiMaster);
}

IFX_INTERRUPT(qspi3ErISR, 0, ISR_PRIORITY_QSPI3_ER)
{
    IfxQspi_SpiMaster_isrError(&g_Qspi_Cpu.drivers.spiMaster);
}


#elif defined(AurixBoard_TC233)

IFX_INTERRUPT(qspi0TxISR, 0, ISR_PRIORITY_QSPI0_TX)
{
    IfxQspi_SpiMaster_isrTransmit(&g_Qspi_Cpu.drivers.spiMaster);
}

IFX_INTERRUPT(qspi0RxISR, 0, ISR_PRIORITY_QSPI0_RX)
{
    IfxQspi_SpiMaster_isrReceive(&g_Qspi_Cpu.drivers.spiMaster);
}

IFX_INTERRUPT(qspi0ErISR, 0, ISR_PRIORITY_QSPI0_ER)
{
    IfxQspi_SpiMaster_isrError(&g_Qspi_Cpu.drivers.spiMaster);
}

#else
#error AurixBoard selection required.
#endif

