/*
 * GPIO.h
 *
 *  Created on: Oct 2, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_ECU_GPIO_H_
#define LIBRARIES_MYTRICORE_ECU_GPIO_H_

#include <Cpu/Std/Ifx_Types.h>
#include "ConfigurationECU.h"
#include "Port/Std/IfxPort.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Add define here


//Add ENUM here

typedef enum
{
    GPIO_0 = 0,
    GPIO_1,
    GPIO_2,
    GPIO_3,
    GPIO_4,
    GPIO_5,
    GPIO_6,
    GPIO_7,
    GPIO_8,
    GPIO_9,
    GPIO_10,
    GPIO_Num
} App_GPIO_Port;

//Add Struct here

typedef struct
{
    Ifx_P *port;
    uint8 pinIndex;
    IfxPort_State activeState;
    boolean status;
}App_GPIO_OutPin;

//Add export variable here

IFX_EXTERN App_GPIO_OutPin g_App_GPIO[GPIO_Num];

//Add function prototype here

IFX_EXTERN void GPIO_init(void);
IFX_EXTERN void GPIO_on(App_GPIO_Port gpio);
IFX_EXTERN void GPIO_off(App_GPIO_Port gpio);


#endif /* LIBRARIES_MYTRICORE_ECU_GPIO_H_ */

