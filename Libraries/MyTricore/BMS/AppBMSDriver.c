/*
 * AppBMSDriver.c
 *
 *  Created on: Oct 9, 2020
 *      Author: weeti
 */


#include "AppBMSDriver.h"
#include "LTC6813.h"

AppBMSData g_AppBMS_data;

void AppBMS_ConvertCellVoltage()
{
	char str[32];
	int i,j,roll;

    LTC6813_init_cfg(LTC6813_TOTAL_IC,bms_ic);
    LTC6813_init_reg_limits(LTC6813_TOTAL_IC, bms_ic);

    bms_config.REFON=REF_ON_ALWAYS;
    bms_config.ADCOPT=ADCOPT_FAST;
    bms_config.gpio_pull_down[0]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[1]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[2]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[3]=PULL_DOWN_ON;
    bms_config.gpio_pull_down[4]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[5]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[6]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[7]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[8]=PULL_DOWN_OFF;
    bms_config.dccBits[0]=DISCHARGE_DISABLE;
    bms_config.dccBits[1]=DISCHARGE_DISABLE;
    bms_config.dccBits[2]=DISCHARGE_DISABLE;
    bms_config.dccBits[3]=DISCHARGE_DISABLE;
    bms_config.dccBits[4]=DISCHARGE_DISABLE;
    bms_config.dccBits[5]=DISCHARGE_DISABLE;
    bms_config.dccBits[6]=DISCHARGE_DISABLE;
	bms_config.dccBits[7]=DISCHARGE_DISABLE;
	bms_config.dccBits[8]=DISCHARGE_DISABLE;
	bms_config.dccBits[9]=DISCHARGE_DISABLE;
	bms_config.dccBits[10]=DISCHARGE_DISABLE;
	bms_config.dccBits[11]=DISCHARGE_DISABLE;
    bms_config.dccBits[12]=DISCHARGE_DISABLE;
    bms_config.dccBits[13]=DISCHARGE_DISABLE;
    bms_config.dccBits[14]=DISCHARGE_DISABLE;
    bms_config.dccBits[15]=DISCHARGE_DISABLE;
    bms_config.dccBits[16]=DISCHARGE_DISABLE;
    bms_config.dccBits[17]=DISCHARGE_DISABLE;
    bms_config.dccBits[18]=DISCHARGE_DISABLE;
    bms_config.dctoBits=DCTO_DISABLE;
    bms_config.UV=33000;
    bms_config.OV=41000;
    bms_config.FDRF=NORMAL_DR;
    bms_config.DTMEN=DTM_DISABLE;
    bms_config.ps=PS_ALL;

    for(i=0;i<LTC6813_TOTAL_IC;i++)
    {
    	LTC6813_set_cfgr(i,bms_ic,bms_config);
    }

    //LTC6813_reset_crc_count(1,bms_ic);
    LTC6813_init_reg_limits(LTC6813_TOTAL_IC,bms_ic);
    LTC6813_wakeup(LTC6813_TOTAL_IC);
    LTC6813_wrcfg(LTC6813_TOTAL_IC,bms_ic);

    LTC6813_wrcfgb(LTC6813_TOTAL_IC,bms_ic);

    LTC6813_rdcfg(LTC6813_TOTAL_IC,bms_ic);

    sint8 perror=LTC6813_rdcfgb(LTC6813_TOTAL_IC,bms_ic);
    if(perror!=0)
    {
        perror=1;
    }
    LTC6813_adcv( MD_7KHZ_3KHZ, //ADC Mode
    		DCP_DISABLED, //Discharge Permit
    		CELL_CH_ALL //Cell Channels to be measured
                     );


    uint32 conv_time=LTC6813_pollAdc();


    LTC6813_rdcv(REG_ALL,LTC6813_TOTAL_IC,bms_ic);
    g_AppBMS_data.totalV=0;
    for(i=0,roll=0;i<LTC6813_TOTAL_IC*18;i++,j++)
    {
    	if(i%18==0)
    	{
    		j=0;
    		roll++;
    	}
    	g_AppBMS_data.cell[i]=bms_ic[roll-1].cells.c_codes[j]*0.0001*50/41.7;//*3.3/2.7;
		g_AppBMS_data.totalV+=g_AppBMS_data.cell[i];
    }
}

void AppBMS_ConvertCellVoltage_Discharge(int cell)
{
    char str[32];
    int i,j,roll;

    LTC6813_init_cfg(LTC6813_TOTAL_IC,bms_ic);
    LTC6813_init_reg_limits(LTC6813_TOTAL_IC, bms_ic);

    bms_config.REFON=REF_ON_ALWAYS;
    bms_config.ADCOPT=ADCOPT_FAST;
    bms_config.gpio_pull_down[0]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[1]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[2]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[3]=PULL_DOWN_ON;
    bms_config.gpio_pull_down[4]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[5]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[6]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[7]=PULL_DOWN_OFF;
    bms_config.gpio_pull_down[8]=PULL_DOWN_OFF;
    bms_config.dccBits[0]=DISCHARGE_DISABLE;
    bms_config.dccBits[1]=DISCHARGE_DISABLE;
    bms_config.dccBits[2]=DISCHARGE_DISABLE;
    bms_config.dccBits[3]=DISCHARGE_DISABLE;
    bms_config.dccBits[4]=DISCHARGE_DISABLE;
    bms_config.dccBits[5]=DISCHARGE_DISABLE;
    bms_config.dccBits[6]=DISCHARGE_DISABLE;
    bms_config.dccBits[7]=DISCHARGE_DISABLE;
    bms_config.dccBits[8]=DISCHARGE_DISABLE;
    bms_config.dccBits[9]=DISCHARGE_DISABLE;
    bms_config.dccBits[10]=DISCHARGE_DISABLE;
    bms_config.dccBits[11]=DISCHARGE_DISABLE;
    bms_config.dccBits[12]=DISCHARGE_DISABLE;
    bms_config.dccBits[13]=DISCHARGE_DISABLE;
    bms_config.dccBits[14]=DISCHARGE_DISABLE;
    bms_config.dccBits[15]=DISCHARGE_DISABLE;
    bms_config.dccBits[16]=DISCHARGE_DISABLE;
    bms_config.dccBits[17]=DISCHARGE_DISABLE;
    bms_config.dccBits[18]=DISCHARGE_DISABLE;
    bms_config.dctoBits=DCTO_DISABLE;
    bms_config.UV=33000;
    bms_config.OV=41000;
    bms_config.FDRF=NORMAL_DR;
    bms_config.DTMEN=DTM_DISABLE;
    bms_config.ps=PS_ALL;

    if(cell!=0)
    {
        bms_config.dccBits[cell]=DISCHARGE_ENABLE;
    }

    for(i=0;i<LTC6813_TOTAL_IC;i++)
    {
        LTC6813_set_cfgr(i,bms_ic,bms_config);
    }

    //LTC6813_reset_crc_count(1,bms_ic);
    LTC6813_init_reg_limits(LTC6813_TOTAL_IC,bms_ic);
    LTC6813_wakeup(LTC6813_TOTAL_IC);
    LTC6813_wrcfg(LTC6813_TOTAL_IC,bms_ic);

    LTC6813_wrcfgb(LTC6813_TOTAL_IC,bms_ic);

    LTC6813_rdcfg(LTC6813_TOTAL_IC,bms_ic);

    sint8 perror=LTC6813_rdcfgb(LTC6813_TOTAL_IC,bms_ic);
    if(perror!=0)
    {
        perror=1;
    }
    LTC6813_adcv( MD_7KHZ_3KHZ, //ADC Mode
            DCP_ENABLED, //Discharge Permit
            CELL_CH_ALL //Cell Channels to be measured
                     );


    uint32 conv_time=LTC6813_pollAdc();


    LTC6813_rdcv(REG_ALL,LTC6813_TOTAL_IC,bms_ic);
    g_AppBMS_data.totalV=0;
    for(i=0,roll=0;i<LTC6813_TOTAL_IC*18;i++,j++)
    {
        if(i%18==0)
        {
            j=0;
            roll++;
        }
        g_AppBMS_data.cell[i]=bms_ic[roll-1].cells.c_codes[j]*0.0001;//*3.3/2.7;
        g_AppBMS_data.totalV+=g_AppBMS_data.cell[i];
    }
}

unsigned char AppBMS_ChkSumData(AppBMSData *bmsData)
{
	int i;
	unsigned char sum=0;
	unsigned char *buffer=bmsData;
	for(i=0;i<sizeof(AppBMSData);i++)
	{
		sum^=buffer[i];
	}
	return sum;
}
